'use strict';
/**
 * Write the unit tests for your transction processor functions here
 */

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection =
    require('composer-client').BusinessNetworkConnection;
const BusinessNetworkDefinition =
    require('composer-common').BusinessNetworkDefinition;
const IdCard = require('composer-common').IdCard;
const MemoryCardStore = require('composer-common').MemoryCardStore;

const path = require('path');
const chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var assert = require('assert');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const namespace = 'pt.irn.bcar';
const assetType = 'Vehicle';
const Person = 'Person';
const Company = 'Company';
const JudicialOfficer = 'JudicialOfficer';
const RegisterAsGuarantee = 'RegisterAsGuarantee';
const CancelGuarantee = 'CancelGuarantee';
const ConfirmGuaranteeCancelation = 'ConfirmGuaranteeCancelation';
const IssuePendingSeizure = 'IssuePendingSeizure';
const Ownership = 'Ownership';
const VALID = 'VALID';
const WAITING = 'WAITING';
const STATE_ACTIVE = 'ACTIVE';

// Required to execute (before) setup function when the test starts
var Utils = require('./before.js');

describe('#'+namespace,() => {

    let businessNetworkConnection;
    let owner;
    let companyOwner;
    let creditor;
    let creditorOwner;
    let judicialOfficer;
    let ownerRelation;

    beforeEach(() => {
      businessNetworkConnection =
        new BusinessNetworkConnection({cardStore: Utils.cardStore});

      return Utils.setup(businessNetworkConnection)
        .then(async () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            // Setting up a creditor
            creditor = factory.newResource(namespace, Company, '111111111');
            creditor.name = "United Bank";
            creditor.address = "Republica 10";

            //Setting up a creditor company owner
            creditorOwner =
                factory.newResource(namespace, Person,'123456789');
            creditorOwner.name = 'Manuel';
            creditorOwner.surname = 'Gomes';
            creditorOwner.address = 'Lisbon';

            const creditorOwnerRel =
                factory
                .newRelationship(namespace, Person, creditorOwner.$identifier);
            var creditorOwners = new Array();
            creditorOwners.push(creditorOwnerRel);
            creditor.owners = creditorOwners;


            // Setting up a company
            owner = factory.newResource(namespace, Company, '222333222');
            owner.name = 'Leasing e Carros LDA';
            owner.address = 'Liberdade 21';

            //Setting up company owner
            companyOwner =
                factory
                .newResource(namespace, Person, '222555888');

            companyOwner.name = 'Richard';
            companyOwner.surname = 'Hammond'
            companyOwner.address = 'Paris';
            const companyOwnerRelation =
                factory
                .newRelationship(namespace, Person, companyOwner.$identifier);
            var owners = new Array();
            owners.push(companyOwnerRelation);
            owner.owners = owners;

            // Setting up a car ownership using a company
            ownerRelation =
                factory.newRelationship(namespace, Company, owner.$identifier);
            const ownership = factory.newConcept(namespace, Ownership);
            owners = new Array();
            owners.push(ownership);
            ownership.owner = ownerRelation;
            ownership.share = 1;
            ownership.state = VALID;

            //Create vehicle and insert it in the blockchain
            const vehicle =
                factory.newResource(namespace, assetType, 'V987654321');

            vehicle.registrationNumber = '00-AA-00';
            vehicle.make = 'Porsche';
            vehicle.typeVersion = '911';
            vehicle.owners = owners;
            vehicle.certificateHolder = ownerRelation;
            vehicle.category = 'M';
            vehicle.vClass = 'LIGHT';
            vehicle.maxLadenMass = 3500;
            const engine = factory.newConcept(namespace, 'Engine');
            engine.capacity = 0;
            engine.fuelType = 'GASOLINE';
            engine.ratedSpeed = 0;
            engine.ein = 'E10';
            vehicle.engine = engine;
            vehicle.state = STATE_ACTIVE;

            const emissions = factory.newConcept(namespace, 'Emissions');
            emissions.emCO = 10;
            emissions.emHC = 10;
            emissions.emNOx = 10;
            emissions.emHCNOx = 10;
            emissions.emCO2 = 10;
            emissions.emFuelConsumption = 7.5;

            vehicle.emissions = emissions;

            // Setting up a judicial Officer
            judicialOfficer =
                factory.newResource(namespace, JudicialOfficer, '234432234');
            judicialOfficer.name = "José";
            judicialOfficer.surname = "Matos";
            judicialOfficer.address = "Rua S. Nunca - Porto";
            judicialOfficer.court = "Tribunal Arbitral de Lisboa";

            //Adding owner to registry
            var promiseCreditor = Utils.addParticipant(
                    businessNetworkConnection,
                    Company,
                    creditor);
            var promiseCredOwner = Utils.addParticipant(
                    businessNetworkConnection,
                    Person,
                    creditorOwner);
            var promiseOwner = Utils.addParticipant(
                    businessNetworkConnection,
                    Company,
                    owner);
            var promiseCompanyOwner = Utils.addParticipant(
                    businessNetworkConnection,
                    Person,
                    companyOwner);

            var promiseJudicialOfficer = Utils.addParticipant(
                businessNetworkConnection,
                JudicialOfficer,
                judicialOfficer);

            var promiseVehicle = businessNetworkConnection
                .getAssetRegistry(namespace + '.' + assetType)
                .then(registry => {
                    return registry.add(vehicle);
                });


            return Promise.all(
                [promiseCreditor,promiseCredOwner,promiseJudicialOfficer,
                promiseOwner,promiseCompanyOwner,promiseVehicle]);
          });
    });

    describe('RegisterAsGuarantee()', () => {
        it('should register a vehicle as Garantee for a loan', () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const registerGarantee =
                factory.newTransaction(namespace,RegisterAsGuarantee);
            const creditorRel =
                factory.newRelationship(namespace,Company,creditor.$identifier);

            registerGarantee.creditor = creditorRel;
            registerGarantee.vin = 'V987654321';
            registerGarantee.registrationNumber = '00-AA-00';
            registerGarantee.make = 'Porsche';
            registerGarantee.type = 'COLLATERAL';
            registerGarantee.totalValue = 400000;
            registerGarantee.penalty = 0.4;
            return Utils.usePersonIdentity(
                    businessNetworkConnection,
                    companyOwner.$identifier,
                () => {
                    return businessNetworkConnection
                        .submitTransaction(registerGarantee)
                        .then(() => {
                            return Utils
                                .getVehicle(
                                    businessNetworkConnection,
                                    registerGarantee.vin)
                                .then( result => {
                                    assert.equal(
                                        result.loan.creditor.$identifier,
                                        registerGarantee.creditor.$identifier);
                                    assert.equal(
                                        result.vin,
                                        registerGarantee.vin);
                                    assert.equal(
                                        result.registrationNumber,
                                        registerGarantee.registrationNumber);
                                    assert.equal(
                                        result.make,
                                        registerGarantee.make);
                                    assert.equal(
                                        result.loan.totalValue,
                                        registerGarantee.totalValue);
                                    assert.equal(
                                        result.loan.penalty,
                                        registerGarantee.penalty);
                                });
                        });
                });
        });

        it('should not register as Garantee if the operation is not done'+
            ' by the owner',() => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();

                const registerGarantee =
                    factory.newTransaction(namespace,RegisterAsGuarantee);
                const creditorRel =
                    factory
                    .newRelationship(namespace,Company,creditor.$identifier);

                registerGarantee.creditor = creditorRel;
                registerGarantee.vin = 'V987654321';
                registerGarantee.registrationNumber = '00-AA-00';
                registerGarantee.make = 'Porsche';
                registerGarantee.type = 'COLLATERAL';

                registerGarantee.totalValue = 400000;
                registerGarantee.penalty = 0.4;
                return Utils.usePersonIdentity(
                        businessNetworkConnection,
                        creditorOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(registerGarantee)
                            .should.be.rejectedWith(
                                'Only the owner is capable of' +
                            ' registering the vehicle as loan garantee.');
                    });
            });

        it('should not register a vehicle as a Garantee if the vehicle'+
            ' is already tied to a loan', () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const registerGarantee =
                factory.newTransaction(namespace,RegisterAsGuarantee);
            const creditorRel =
                factory.newRelationship(namespace,Company,creditor.$identifier);

            // Setting up a second creditor
            const creditor2 = factory.newResource(namespace, Company, '111111112');
            creditor2.name = "Split Bank";
            creditor2.address = "Elias 1";

            //Setting up a second creditor company owner
            const creditorOwner2 =
                factory.newResource(namespace, Person,'123456783');
            creditorOwner2.name = 'João';
            creditorOwner2.surname = 'Gomes';
            creditorOwner2.address = 'Porto';

            const creditor2Rel =
                factory
                .newRelationship(namespace,Company,creditor2.$identifier);

            const creditorOwnerRel =
                factory
                .newRelationship(namespace, Person, creditorOwner2.$identifier);
            var creditorOwners = new Array();
            creditorOwners.push(creditorOwnerRel);
            creditor2.owners = creditorOwners;


            registerGarantee.creditor = creditorRel;
            registerGarantee.vin = 'V987654321';
            registerGarantee.registrationNumber = '00-AA-00';
            registerGarantee.make = 'Porsche';
            registerGarantee.type = 'COLLATERAL';

            registerGarantee.totalValue = 400000;
            registerGarantee.penalty = 0.4;
            return Utils.usePersonIdentity(
                    businessNetworkConnection,
                    companyOwner.$identifier,
                () => {
                    return businessNetworkConnection
                        .submitTransaction(registerGarantee)
                        .then(() => {
                            registerGarantee.creditor = creditor2Rel;
                            registerGarantee.totalValue = 10000;
                            registerGarantee.penalty = 0.2;
                            return businessNetworkConnection
                                .submitTransaction(registerGarantee)
                                .should.be.rejectedWith(
                                    'Vehicle with registration number ' +
                                    registerGarantee.registrationNumber +
                                    ' was already registered as loan garantee.');
                        });
                });
            });

        it('should require authorization from a creditor to remove vehicle as'+
            ' Garantee of a loan', () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const registerGarantee =
                factory.newTransaction(namespace,RegisterAsGuarantee);
            const creditorRel =
                factory.newRelationship(namespace,Company,creditor.$identifier);

            registerGarantee.creditor = creditorRel;
            registerGarantee.vin = 'V987654321';
            registerGarantee.registrationNumber = '00-AA-00';
            registerGarantee.make = 'Porsche';
            registerGarantee.type = 'COLLATERAL';
            registerGarantee.totalValue = 400000;
            registerGarantee.penalty = 0.4;
            return Utils.usePersonIdentity(
                    businessNetworkConnection,
                    companyOwner.$identifier,
                () => {
                    return businessNetworkConnection
                        .submitTransaction(registerGarantee)
                        .then(() => {
                            const cancelGuarantee =
                                factory
                                .newTransaction(namespace,CancelGuarantee);
                            cancelGuarantee.vin = registerGarantee.vin;
                            cancelGuarantee.registrationNumber =
                                registerGarantee.registrationNumber;
                            cancelGuarantee.make = registerGarantee.make;
                            return businessNetworkConnection
                                .submitTransaction(cancelGuarantee)
                                .then( () => {
                                    return Utils
                                        .getVehicle(
                                            businessNetworkConnection,
                                            registerGarantee.vin)
                                        .then( result => {
                                            assert.equal(
                                                result.loan.creditor.$identifier,
                                                registerGarantee.creditor.$identifier);
                                            assert.equal(
                                                result.vin,
                                                registerGarantee.vin);
                                            assert.equal(
                                                result.registrationNumber,
                                                registerGarantee.registrationNumber);
                                            assert.equal(
                                                result.make,
                                                registerGarantee.make);
                                            assert.equal(
                                                result.loan.totalValue,
                                                registerGarantee.totalValue);
                                            assert.equal(
                                                result.loan.penalty,
                                                registerGarantee.penalty);
                                            assert.equal(
                                                result.loan.waitingCancelation,
                                                true);
                                        });
                                })
                        });
                });
            });

        it('should remove the vehicle as Garantee of a loan after creditor ' +
            'confirmation Garantee of a loan', () => {
                const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

                const registerGarantee =
                    factory.newTransaction(namespace,RegisterAsGuarantee);
                const creditorRel =
                    factory.newRelationship(namespace,Company,creditor.$identifier);

                registerGarantee.creditor = creditorRel;
                registerGarantee.vin = 'V987654321';
                registerGarantee.registrationNumber = '00-AA-00';
                registerGarantee.make = 'Porsche';
                registerGarantee.type = 'COLLATERAL';
                registerGarantee.totalValue = 400000;
                registerGarantee.penalty = 0.4;
                return Utils.usePersonIdentity(
                        businessNetworkConnection,
                        companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(registerGarantee)
                            .then(() => {
                                const cancelGuarantee =
                                    factory
                                    .newTransaction(namespace,CancelGuarantee);
                                cancelGuarantee.vin = registerGarantee.vin;
                                cancelGuarantee.registrationNumber =
                                    registerGarantee.registrationNumber;
                                cancelGuarantee.make = registerGarantee.make;
                                return businessNetworkConnection
                                    .submitTransaction(cancelGuarantee);
                                    });
                }).then(() => {
                    // FIXME use admin identity before changing to other identity
                    return businessNetworkConnection
                        .connect('admin@bcar-network')
                        .then(() => {
                            return Utils.usePersonIdentity(
                                businessNetworkConnection,
                                creditorOwner.$identifier,
                                () => {
                                    const confirmCancel =
                                        factory
                                        .newTransaction(
                                            namespace,
                                            ConfirmGuaranteeCancelation);
                                        confirmCancel.vin =
                                            registerGarantee.vin;
                                        confirmCancel
                                        .registrationNumber = registerGarantee.registrationNumber;
                                        confirmCancel.make =
                                            registerGarantee.make;

                                    return businessNetworkConnection
                                        .submitTransaction(confirmCancel)
                                        .then(() => {
                                            return Utils
                                                .getVehicle(
                                                    businessNetworkConnection,
                                                    registerGarantee.vin)
                                                .then( result => {
                                                    assert.equal(
                                                        result.loan,
                                                        null);
                                                });
                                    });
                                });
                        });
                });

            });

        it('should not require authorization to remove vehicle as'+
            ' Garantee of a loan if done by the creditor',() => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const registerGarantee =
                factory.newTransaction(namespace,RegisterAsGuarantee);
            const creditorRel =
                factory.newRelationship(namespace,Company,creditor.$identifier);

            registerGarantee.creditor = creditorRel;
            registerGarantee.vin = 'V987654321';
            registerGarantee.registrationNumber = '00-AA-00';
            registerGarantee.make = 'Porsche';
            registerGarantee.type = 'COLLATERAL';
            registerGarantee.totalValue = 400000;
            registerGarantee.penalty = 0.4;
            return Utils.usePersonIdentity(
                    businessNetworkConnection,
                    companyOwner.$identifier,
                () => {
                    return businessNetworkConnection
                        .submitTransaction(registerGarantee);
                }).then(() =>{
                    const cancelGuarantee =
                        factory
                        .newTransaction(namespace,CancelGuarantee);
                    cancelGuarantee.vin = registerGarantee.vin;
                    cancelGuarantee.registrationNumber =
                        registerGarantee.registrationNumber;
                    cancelGuarantee.make = registerGarantee.make;

                    return businessNetworkConnection
                        .connect('admin@bcar-network')
                        .then(() => {

                            return Utils.usePersonIdentity(
                                businessNetworkConnection,
                                creditorOwner.$identifier,
                                () => {
                                    return businessNetworkConnection
                                        .submitTransaction(cancelGuarantee)
                                        .then( () => {
                                            return Utils
                                                .getVehicle(
                                                    businessNetworkConnection,
                                                    registerGarantee.vin)
                                                .then( result => {
                                                    assert.equal(
                                                        result.loan,
                                                        null);
                                                });
                                        });
                                });
                        });
                });
        });
        it('should not register a car as garantee that is subject to a pending seizure', async () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            let pendingSeizure =
                factory.newTransaction(namespace,IssuePendingSeizure);
            const creditorRel =
                factory.newRelationship(namespace,Company,creditor.$identifier);

            pendingSeizure.owner = ownerRelation;
            pendingSeizure.creditor = creditorRel;
            pendingSeizure.totalValue = 150000;
            pendingSeizure.vin = 'V987654321';
            pendingSeizure.registrationNumber = '00-AA-00';
            pendingSeizure.make = 'Porsche';

            await Utils.useIdentity(
                businessNetworkConnection,
                JudicialOfficer,
                judicialOfficer.$identifier,
                () =>{
                    return businessNetworkConnection
                        .submitTransaction(pendingSeizure);
                });

            const registerGarantee =
                factory.newTransaction(namespace,RegisterAsGuarantee);


            registerGarantee.creditor = creditorRel;
            registerGarantee.vin = 'V987654321';
            registerGarantee.registrationNumber = '00-AA-00';
            registerGarantee.make = 'Porsche';
            registerGarantee.type = 'COLLATERAL';
            registerGarantee.totalValue = 400000;
            registerGarantee.penalty = 0.4;
            return businessNetworkConnection
                .connect('admin@bcar-network')
                .then(() => {
                    return Utils.usePersonIdentity(
                        businessNetworkConnection,
                        companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(registerGarantee)
                            .should.be
                            .rejectedWith('Vehicle should not have a pending seizure in place');
                    });
                });
        });

    });
});