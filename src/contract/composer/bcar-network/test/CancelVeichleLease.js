'use strict';

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection =
	require('composer-client').BusinessNetworkConnection;

const path = require('path');
const chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var assert = require('assert');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const namespace = 'pt.irn.bcar';
const assetType = 'Vehicle';
const Person = 'Person';
const Company = 'Company';
const CreateLease = 'CreateLease';
const ConfirmLeaseTermination =  'ConfirmLeaseTermination';
const ConfirmLease = "ConfirmLease";
const CancelLease = 'CancelLease';
const CancelLeaseTermination = 'CancelLeaseTermination';
const Ownership = 'Ownership';
const JudicialOfficer = "JudicialOfficer";
const VALID = 'VALID';
const WAITING = 'WAITING';
const WAITINGCANCELATION = 'WAITINGCANCELATION';
const STATE_ACTIVE = 'ACTIVE';

// Required to execute (before) setup function when the test starts
var Utils = require('./before.js');

describe('#'+namespace,() => {
	let businessNetworkConnection;
	let owner;
	let companyOwner;
    let vehicle;
    let lessee;

	beforeEach(() => {
        businessNetworkConnection =
            new BusinessNetworkConnection({cardStore: Utils.cardStore});

        return Utils.setup(businessNetworkConnection)
            .then(async () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();

        		 // Setting up a company
                owner = factory.newResource(namespace, Company, '222333222');
                owner.name = 'Leasing e Carros LDA';
                owner.address = 'Liberdade 21';

                //Setting up company owner
                companyOwner =
                    factory
                    .newResource(namespace, Person, '222555888');

                companyOwner.name = 'Richard';
                companyOwner.surname = 'Hammond'
                companyOwner.address = 'Paris';
                const companyOwnerRelation =
                    factory
                    .newRelationship(
                        namespace, Person, companyOwner.$identifier);
                var owners = new Array();
                owners.push(companyOwnerRelation);
                owner.owners = owners;

                // Setting up a car ownership using a company
                const ownerRelation =
                    factory
                    .newRelationship(namespace, Company, owner.$identifier);
                const ownership = factory.newConcept(namespace, Ownership);
                owners = new Array();
                owners.push(ownership);
                ownership.owner = ownerRelation;
                ownership.share = 1;
                ownership.state = VALID;

                lessee = factory.newResource(namespace, Person, '123456789');
                lessee.name = 'James';
                lessee.surname = 'May';
                lessee.address = 'London';

                //Create vehicle and insert it in the blockchain
                vehicle =
                    factory.newResource(namespace, assetType, 'V987654321');

                vehicle.registrationNumber = '00-AA-00';
                vehicle.make = 'Porsche';
                vehicle.typeVersion = '911';
                vehicle.owners = owners;
                vehicle.certificateHolder = ownerRelation;
                vehicle.category = 'M';
                vehicle.vClass = 'LIGHT';
                vehicle.maxLadenMass = 3500;
                const engine = factory.newConcept(namespace, 'Engine');
                engine.capacity = 0;
                engine.fuelType = 'GASOLINE';
                engine.ratedSpeed = 0;
                engine.ein = 'E10';
                vehicle.engine = engine;
                vehicle.state = STATE_ACTIVE;

                const emissions = factory.newConcept(namespace, 'Emissions');
                emissions.emCO = 10;
                emissions.emHC = 10;
                emissions.emNOx = 10;
                emissions.emHCNOx = 10;
                emissions.emCO2 = 10;
                emissions.emFuelConsumption = 7.5;

                vehicle.emissions = emissions;

                var promiseOwner = Utils.addParticipant(
                            businessNetworkConnection,
                            Company,
                            owner);

                var promiseLessee = Utils.addParticipant(
                            businessNetworkConnection,
                            Person,
                            lessee);
                var promiseCompanyOwner = Utils.addParticipant(
                            businessNetworkConnection,
                            Person,
                            companyOwner);

                var promiseVehicle = businessNetworkConnection
        	        .getAssetRegistry(namespace + '.' + assetType)
        	        .then(registry => {
        	            return registry.add(vehicle);
        	        });

        	    return Promise.all(
                        [promiseOwner,promiseLessee,
                        promiseCompanyOwner,promiseVehicle]);
            });
	});

	describe('CancelVehicleLease()', () => {
		it('should  allow a lessee to cancel a lease contract and allow the' +
            ' lessor to confirm the lease termination being the' +
            ' lessee a Person', async () => {
			const factory =
				businessNetworkConnection.getBusinessNetwork().getFactory();

            const createLease =
                factory.newTransaction(namespace,CreateLease);
            createLease.startDate = new Date();
            createLease.endDate =
                new Date(
                    createLease.startDate.getFullYear()+5,
                    createLease.startDate.getMonth(),
                    createLease.startDate.getDay(),
                    createLease.startDate.getHours(),
                    createLease.startDate.getMinutes(),
                    createLease.startDate.getSeconds(),
                    createLease.startDate.getMilliseconds());
            createLease.vin = 'V987654321';
            createLease.make = 'Porsche';
            createLease.registrationNumber = '00-AA-00';
            createLease.totalValue= 75000;
            createLease.lessee =
                factory.newRelationship(namespace,Person,lessee.$identifier);

            await Utils.useIdentity(
                businessNetworkConnection,
                Person,
                companyOwner.$identifier,
                () => {
                    return businessNetworkConnection
                        .submitTransaction(createLease);
            })
            const confirmLease = factory.newTransaction(namespace,ConfirmLease);
            confirmLease.vin =  'V987654321';
            confirmLease.make = 'Porsche';
            confirmLease.registrationNumber = '00-AA-00';
            confirmLease.totalValue = 75000;
            confirmLease.lessee =
                factory.newRelationship(namespace,Person,lessee.$identifier);
            confirmLease.lessor =
                factory.newRelationship(namespace,Company,owner.$identifier);

            const terminateLease =
                factory.newTransaction(namespace,CancelLease);
            terminateLease.vin = confirmLease.vin ;
            terminateLease.make = confirmLease.make ;
            terminateLease.registrationNumber = confirmLease.registrationNumber;
            terminateLease.totalValue = confirmLease.totalValue;

            terminateLease.lessee = confirmLease.lessee;
            terminateLease.lessor = confirmLease.lessor;

            await businessNetworkConnection
                .connect('admin@bcar-network')
                .then(() => {
                    return Utils
                        .usePersonIdentity(
                            businessNetworkConnection,
                            lessee.$identifier
                            ,() => {
                                return businessNetworkConnection
                                    .submitTransaction(confirmLease)
                                    .then( () => {
                                        return businessNetworkConnection
                                            .submitTransaction(terminateLease)
                                            .then(() => {
                                                return Utils
                                                    .getVehicle(
                                                        businessNetworkConnection,
                                                        terminateLease.vin)
                                                    .then( result => {
                                                        assert.notEqual(
                                                            result.lease,
                                                            undefined);
                                                        assert.equal(
                                                            result.lease.state,
                                                            WAITINGCANCELATION);
                                                    });
                                            });
                                    });
                            });
                });

			return businessNetworkConnection
                .connect('admin@bcar-network')
				.then(() => {
					return businessNetworkConnection
                        .connect(companyOwner.$identifier+'@bcar-network')
						.then(() => {
							// Issue Transaction for confirming termination
								const confirmLeaseTermination =
									factory
									.newTransaction(
										namespace,ConfirmLeaseTermination);
                                confirmLeaseTermination.vin = vehicle.vin;
                                confirmLeaseTermination.make = vehicle.make;
                                confirmLeaseTermination.registrationNumber =
                                    vehicle.registrationNumber;

								return businessNetworkConnection
									.submitTransaction(confirmLeaseTermination)
									.then( () => {
										return Utils
											.getVehicle(
												businessNetworkConnection,
												terminateLease.vin)
											.then( result => {
												assert.equal(
													result.lease,
													undefined);
											});
									});
							});
				});
		});

        it('should allow a lessor to cancel a lease contract and allow the' +
            ' lessee to confirm lease termination if the leasee is a Company',
             async () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();
                const lesseeCompany =
                    factory.newResource(namespace, Company, '968364154');
                lesseeCompany.name = "Just Do IT";
                lesseeCompany.address = 'Republica 20 - Lisboa';

                const companyOwnerRelation =
                    factory
                    .newRelationship(namespace,Person, lessee.$identifier);
                var owners = new Array();
                owners.push(companyOwnerRelation);
                lesseeCompany.owners = owners;

                await Utils.addParticipant(
                            businessNetworkConnection,
                            Company,
                            lesseeCompany);

                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        lesseeCompany.$identifier);

                await Utils.useIdentity(
                    businessNetworkConnection,
                    Person,
                    companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease);
                });
                const confirmLease = factory.newTransaction(namespace,ConfirmLease);
                confirmLease.vin =  'V987654321';
                confirmLease.make = 'Porsche';
                confirmLease.registrationNumber = '00-AA-00';
                confirmLease.totalValue = 75000;
                confirmLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);
                confirmLease.lessor =
                    factory.newRelationship(namespace,Company,owner.$identifier);
    
                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = confirmLease.vin ;
                terminateLease.make = confirmLease.make ;
                terminateLease.registrationNumber = confirmLease.registrationNumber;
                terminateLease.totalValue = confirmLease.totalValue;
    
                terminateLease.lessee = confirmLease.lessee;
                terminateLease.lessor = confirmLease.lessor;
    
                return businessNetworkConnection
                    .connect('admin@bcar-network')
                    .then(() => {
                        return Utils.usePersonIdentity(businessNetworkConnection, lessee.$identifier,
                            () => {
                                return businessNetworkConnection
                                    .submitTransaction(confirmLease)
                                    .then( () => {
                                        return businessNetworkConnection
                                            .connect('admin@bcar-network')
                                            .then(() => {
                                                return businessNetworkConnection
                                                    .connect(companyOwner.$identifier+'@bcar-network') 
                                                    .then( 
                                                        () => {
                                                        return businessNetworkConnection
                                                            .submitTransaction(terminateLease)
                                                            .then(() => {
                                                                return Utils
                                                                    .getVehicle(
                                                                        businessNetworkConnection,
                                                                        terminateLease.vin)
                                                                    .then( result => {
                                                                        assert.notEqual(
                                                                            result.lease,
                                                                            undefined);
                                                                        assert.equal(
                                                                            result.lease.state,
                                                                            WAITINGCANCELATION);
                                                                    });
                                                            });
                                                })
                                            });
                                    });
                            });
                    })
                    .then(() => {

                        return businessNetworkConnection
                            .connect('admin@bcar-network')
                            .then( () => {
                                return businessNetworkConnection
                                    .connect(lessee.$identifier+'@bcar-network') 
                                    .then( () => {
                                        // Issue Transaction for confirming termination
                                            const confirmLeaseTermination =
                                                factory
                                                .newTransaction(
                                                    namespace,ConfirmLeaseTermination);
                                            confirmLeaseTermination.vin = vehicle.vin;
                                            confirmLeaseTermination.make =
                                                vehicle.make;
                                            confirmLeaseTermination.registrationNumber =
                                                vehicle.registrationNumber;

                                            return businessNetworkConnection
                                                .submitTransaction(
                                                    confirmLeaseTermination)
                                                .then( () => {
                                                    return Utils
                                                        .getVehicle(
                                                            businessNetworkConnection,
                                                            terminateLease.vin)
                                                        .then( result => {
                                                            assert.equal(
                                                                result.lease,
                                                                undefined);
                                                        });
                                                });
                                        });
                            });

                    });
            });

        it('should allow a lessor to cancel a lease contract and allow the' +
            ' lessee to confirm lease termination if the leasee is a Person',
            async () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();


                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory
                    .newRelationship(namespace,Person,lessee.$identifier);

                await Utils.useIdentity(
                    businessNetworkConnection,
                    Person,
                    companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease);
                });

                const confirmLease = factory.newTransaction(namespace,ConfirmLease);
                confirmLease.vin =  'V987654321';
                confirmLease.make = 'Porsche';
                confirmLease.registrationNumber = '00-AA-00';
                confirmLease.totalValue = 75000;
                confirmLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);
                confirmLease.lessor =
                    factory.newRelationship(namespace,Company,owner.$identifier);

                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = confirmLease.vin ;
                terminateLease.make = confirmLease.make ;
                terminateLease.registrationNumber = confirmLease.registrationNumber;
                terminateLease.totalValue = confirmLease.totalValue;

                terminateLease.lessee = confirmLease.lessee;
                terminateLease.lessor = confirmLease.lessor;

                return businessNetworkConnection
                    .connect('admin@bcar-network')
                    .then( () => {
                        return Utils.usePersonIdentity(businessNetworkConnection, lessee.$identifier,
                            () => {
                                return businessNetworkConnection
                                    .submitTransaction(confirmLease)
                                    .then( () => {
                                        
                                        return businessNetworkConnection
                                            .connect(companyOwner.$identifier+'@bcar-network')
                                            .then(() => {
                                                return businessNetworkConnection
                                                    .submitTransaction(terminateLease)
                                                    .then(() => {
                                                        return Utils
                                                            .getVehicle(
                                                                businessNetworkConnection,
                                                                terminateLease.vin)
                                                            .then( result => {
                                                                assert.notEqual(
                                                                    result.lease,
                                                                    undefined);
                                                                assert.equal(
                                                                    result.lease.state,
                                                                    WAITINGCANCELATION);
                                                            });
                                            });
                                });
                            });
                        });
                    })
                    .then(() => {
                        return businessNetworkConnection
                            .connect('admin@bcar-network')
                            .then( () => {
                                return businessNetworkConnection
                                    .connect(lessee.$identifier+'@bcar-network')
                                    .then(                                        
                                        () => {
                                        // Issue Transaction for confirming termination
                                            const confirmLeaseTermination =
                                                factory
                                                .newTransaction(
                                                    namespace,ConfirmLeaseTermination);
                                            confirmLeaseTermination.vin = vehicle.vin;
                                            confirmLeaseTermination.make =
                                                vehicle.make;
                                            confirmLeaseTermination.registrationNumber =
                                                vehicle.registrationNumber;

                                            return businessNetworkConnection
                                                .submitTransaction(
                                                    confirmLeaseTermination)
                                                .then( () => {
                                                    return Utils
                                                        .getVehicle(
                                                            businessNetworkConnection,
                                                            terminateLease.vin)
                                                        .then( result => {
                                                            assert.equal(
                                                                result.lease,
                                                                undefined);
                                                        });
                                                });
                                });
                            });

                    });
        });

        it('should  allow a lessee to cancel a lease contract and allow the' +
            ' lessor to confirm the lease termination being the' +
            ' lessee a Company',
            async () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();
                const lesseeCompany =
                    factory.newResource(namespace, Company, '968364154');
                lesseeCompany.name = "Just Do IT";
                lesseeCompany.address = 'Republica 20 - Lisboa';

                const companyOwnerRelation =
                    factory
                    .newRelationship(namespace,Person, lessee.$identifier);
                var owners = new Array();
                owners.push(companyOwnerRelation);
                lesseeCompany.owners = owners;


                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        lesseeCompany.$identifier);

                await Utils.addParticipant(
                    businessNetworkConnection,
                    Company,
                    lesseeCompany);

                await Utils.useIdentity(
                    businessNetworkConnection,
                    Person,
                    companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease);
                });

                const confirmLease = factory.newTransaction(namespace,ConfirmLease);
                confirmLease.vin =  'V987654321';
                confirmLease.make = 'Porsche';
                confirmLease.registrationNumber = '00-AA-00';
                confirmLease.totalValue = 75000;
                confirmLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);
                confirmLease.lessor =
                    factory.newRelationship(namespace,Company,owner.$identifier);

                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = confirmLease.vin ;
                terminateLease.make = confirmLease.make ;
                terminateLease.registrationNumber = confirmLease.registrationNumber;
                terminateLease.totalValue = confirmLease.totalValue;

                terminateLease.lessee = confirmLease.lessee;
                terminateLease.lessor = confirmLease.lessor;

                return businessNetworkConnection
                    .connect('admin@bcar-network')
                    .then( () => {
                        return  Utils
                            .useIdentity(
                                businessNetworkConnection,
                                Person,
                                lessee.$identifier,
                                () => {
                                    return businessNetworkConnection
                                        .submitTransaction(confirmLease)
                                        .then(() => {
                                            return businessNetworkConnection
                                            .submitTransaction(terminateLease)
                                            .then(() => {
                                                return Utils
                                                    .getVehicle(
                                                        businessNetworkConnection,
                                                        terminateLease.vin)
                                                    .then( result => {
                                                        assert.notEqual(
                                                            result.lease,
                                                            undefined);
                                                        assert.equal(
                                                            result.lease.state,
                                                            WAITINGCANCELATION);
                                                    });
                                            })
    
                                        });
                                });
                    })
                    .then(() => {
                        return businessNetworkConnection
                            .connect('admin@bcar-network')
                            .then( () => {
                                return businessNetworkConnection
                                    .connect(companyOwner.$identifier+'@bcar-network')
                                    .then( () => {
                                        // Issue Transaction for confirming
                                        // termination
                                            const confirmLeaseTermination =
                                                factory
                                                .newTransaction(
                                                    namespace,
                                                    ConfirmLeaseTermination);
                                            confirmLeaseTermination.vin =
                                                vehicle.vin;
                                            confirmLeaseTermination.make =
                                                vehicle.make;
                                            confirmLeaseTermination
                                                .registrationNumber =
                                                    vehicle.registrationNumber;

                                            return businessNetworkConnection
                                                .submitTransaction(
                                                    confirmLeaseTermination)
                                                .then( () => {
                                                    return Utils
                                                        .getVehicle(
                                                            businessNetworkConnection,
                                                            terminateLease.vin)
                                                        .then( result => {
                                                            assert.equal(
                                                                result.lease,
                                                                undefined);
                                                        });
                                                });
                                        });
                            });

                    });
            });


       it('should not allow a person who is not the owner nor the lessee to'+
        ' issue a lease termination transaction', async () => {
        	const factory =
        		businessNetworkConnection.getBusinessNetwork().getFactory();

        	var wrongPerson =
        		factory.newResource( namespace, Person, '989676555');
        	wrongPerson.name = 'Tom';
        	wrongPerson.surname = 'Hardy';
        	wrongPerson.address = 'NYC';

            const createLease =
                    factory.newTransaction(namespace,CreateLease);
            createLease.startDate = new Date();
            createLease.endDate =
                new Date(
                    createLease.startDate.getFullYear()+5,
                    createLease.startDate.getMonth(),
                    createLease.startDate.getDay(),
                    createLease.startDate.getHours(),
                    createLease.startDate.getMinutes(),
                    createLease.startDate.getSeconds(),
                    createLease.startDate.getMilliseconds());
            createLease.vin = 'V987654321';
            createLease.make = 'Porsche';
            createLease.registrationNumber = '00-AA-00';
            createLease.totalValue= 75000;
            createLease.lessee =
                factory
                .newRelationship(namespace,Person,lessee.$identifier);

            await Utils.useIdentity(
                businessNetworkConnection,
                Person,
                companyOwner.$identifier,
                () => {
                    return businessNetworkConnection
                        .submitTransaction(createLease);
            });

            const confirmLease = factory.newTransaction(namespace,ConfirmLease);
            confirmLease.vin =  'V987654321';
            confirmLease.make = 'Porsche';
            confirmLease.registrationNumber = '00-AA-00';
            confirmLease.totalValue = 75000;
            confirmLease.lessee =
                factory.newRelationship(namespace,Person,lessee.$identifier);
            confirmLease.lessor =
                factory.newRelationship(namespace,Company,owner.$identifier);

            const terminateLease =
                factory.newTransaction(namespace,CancelLease);
            terminateLease.vin = confirmLease.vin ;
            terminateLease.make = confirmLease.make ;
            terminateLease.registrationNumber = confirmLease.registrationNumber;
            terminateLease.totalValue = confirmLease.totalValue;

            terminateLease.lessee = confirmLease.lessee;
            terminateLease.lessor = confirmLease.lessor;

        	return businessNetworkConnection
                .connect('admin@bcar-network')
                .then( () => {
                    return Utils
                    .usePersonIdentity(
                        businessNetworkConnection,
                        lessee.$identifier, () => {
                            return businessNetworkConnection
                                .submitTransaction(confirmLease);
                        });
                })
                .then( () => {
                    return businessNetworkConnection
                        .connect('admin@bcar-network')
                        .then( async () => {
                            await Utils
                                .addParticipant(
                                    businessNetworkConnection,
                                    Person,
                                    wrongPerson);
                            
                            return Utils
                                .usePersonIdentity(
                                    businessNetworkConnection,
                                    wrongPerson.$identifier,
                                    () => {
                                        return businessNetworkConnection
                                            .submitTransaction(terminateLease)
                                            .should.eventually.be
                                            .rejectedWith(
                                                'Only the owner or the'+
                                                ' lessee can issue a lease termination.');
                                    });
                            });
                });
        });

	   it('should not allow a person who is not the owner nor the lessee to'+
        ' issue a confirm lease termination transaction',
        async () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();

                var wrongPerson =
        			factory.newResource( namespace, Person, '989676555');
	        	wrongPerson.name = 'Tom';
	        	wrongPerson.surname = 'Hardy';
	        	wrongPerson.address = 'NYC';

                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory
                    .newRelationship(namespace,Person,lessee.$identifier);

                await Utils
        			.addParticipant(
	        			businessNetworkConnection,
	        			Person,
	        			wrongPerson);

                await Utils.useIdentity(
                    businessNetworkConnection,
                    Person,
                    companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease);
                });
                
                const confirmLease = factory.newTransaction(namespace,ConfirmLease);
                confirmLease.vin =  'V987654321';
                confirmLease.make = 'Porsche';
                confirmLease.registrationNumber = '00-AA-00';
                confirmLease.totalValue = 75000;
                confirmLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);
                confirmLease.lessor =
                    factory.newRelationship(namespace,Company,owner.$identifier);
    
                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = confirmLease.vin ;
                terminateLease.make = confirmLease.make ;
                terminateLease.registrationNumber = confirmLease.registrationNumber;
                terminateLease.totalValue = confirmLease.totalValue;
    
                terminateLease.lessee = confirmLease.lessee;
                terminateLease.lessor = confirmLease.lessor;
                
                await businessNetworkConnection
                    .connect('admin@bcar-network')
                    .then( () => {
                        return Utils
                        .useIdentity(
                            businessNetworkConnection,
                            Person,
                            lessee.$identifier, () => {
                                return businessNetworkConnection
                                    .submitTransaction(confirmLease);
                            });
                    });
                
    
                return businessNetworkConnection
                    .connect('admin@bcar-network')
                    .then(() => {
                        return businessNetworkConnection
                            .connect(companyOwner.$identifier+'@bcar-network')
                            .then(() => {
                                    return businessNetworkConnection
                                        .submitTransaction(terminateLease)
                                        .then(() => {
                                            return Utils
                                                .getVehicle(
                                                    businessNetworkConnection,
                                                    terminateLease.vin)
                                                .then( result => {
                                                    assert.notEqual(
                                                        result.lease,
                                                        undefined);
                                                    assert.equal(
                                                        result.lease.state,
                                                        WAITINGCANCELATION);
                                                });
                                        });

                                });
                    })
                    .then(() => {
                        return  businessNetworkConnection
                            .connect('admin@bcar-network')
                            .then(() => {
                                return Utils
                                    .useIdentity(
                                        businessNetworkConnection,
                                        Person,
                                        wrongPerson.$identifier,
                                        () => {
                                        // Issue Transaction for confirming termination
                                            const confirmLeaseTermination =
                                                factory
                                                .newTransaction(
                                                    namespace,
                                                    ConfirmLeaseTermination);
                                            confirmLeaseTermination.vin =
                                                vehicle.vin;
                                            confirmLeaseTermination.make =
                                                vehicle.make;
                                            confirmLeaseTermination
                                                .registrationNumber =
                                                    vehicle.registrationNumber;

                                            return businessNetworkConnection
                                                .submitTransaction(
                                                    confirmLeaseTermination)
                                                .should.eventually.be
                                                .rejectedWith(
                                                    'Only the owner or the ' +
                                                    'lessee can confirm a ' +
                                                    'lease termination.');
                                        });
                            });
                    });
        });

       it('should not allow a lessor to cancel a lease contract and confirm'+
        ' the lease cancelation', async () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();


            const createLease =
                factory.newTransaction(namespace,CreateLease);
            createLease.startDate = new Date();
            createLease.endDate =
                new Date(
                    createLease.startDate.getFullYear()+5,
                    createLease.startDate.getMonth(),
                    createLease.startDate.getDay(),
                    createLease.startDate.getHours(),
                    createLease.startDate.getMinutes(),
                    createLease.startDate.getSeconds(),
                    createLease.startDate.getMilliseconds());
            createLease.vin = 'V987654321';
            createLease.make = 'Porsche';
            createLease.registrationNumber = '00-AA-00';
            createLease.totalValue= 75000;
            createLease.lessee =
                factory.newRelationship(namespace,Person,lessee.$identifier);

            await Utils.useIdentity(
                businessNetworkConnection,
                Person,
                companyOwner.$identifier,
                () => {
                    return businessNetworkConnection
                        .submitTransaction(createLease);
                        
            });

            
            const confirmLease = factory.newTransaction(namespace,ConfirmLease);
            confirmLease.vin =  'V987654321';
            confirmLease.make = 'Porsche';
            confirmLease.registrationNumber = '00-AA-00';
            confirmLease.totalValue = 75000;
            confirmLease.lessee =
                factory.newRelationship(namespace,Person,lessee.$identifier);
            confirmLease.lessor =
                factory.newRelationship(namespace,Company,owner.$identifier);

            const terminateLease =
                factory.newTransaction(namespace,CancelLease);
            terminateLease.vin = confirmLease.vin ;
            terminateLease.make = confirmLease.make ;
            terminateLease.registrationNumber = confirmLease.registrationNumber;
            terminateLease.totalValue = confirmLease.totalValue;

            terminateLease.lessee = confirmLease.lessee;
            terminateLease.lessor = confirmLease.lessor;

            return businessNetworkConnection
                .connect('admin@bcar-network')
                .then( async () => {
                    return Utils
                        .useIdentity(
                            businessNetworkConnection,
                            Person,
                            lessee.$identifier, () => {
                                return businessNetworkConnection
                                    .submitTransaction(confirmLease);
                            });
                }).then(
                    () => {
                        return businessNetworkConnection
                            .connect('admin@bcar-network')
                            .then( async () => {
                                return businessNetworkConnection
                                    .connect(companyOwner.$identifier+'@bcar-network')
                                    .then(
                                    () => {
                                        return businessNetworkConnection
                                            .submitTransaction(terminateLease)
                                            .then(() => {
                                                return Utils
                                                    .getVehicle(
                                                        businessNetworkConnection,
                                                        terminateLease.vin)
                                                    .then( result => {
                                                        assert.notEqual(
                                                            result.lease,
                                                            undefined);
                                                        assert.equal(
                                                            result.lease.state,
                                                            WAITINGCANCELATION);
                                                    });
                                            })
                                            .then( () => {
                    
                                                    // Issue Transaction for confirming termination
                                                const confirmLeaseTermination =
                                                    factory
                                                    .newTransaction(
                                                        namespace,ConfirmLeaseTermination);
                                                confirmLeaseTermination.vin = vehicle.vin;
                                                confirmLeaseTermination.make = vehicle.make;
                                                confirmLeaseTermination.registrationNumber =
                                                    vehicle.registrationNumber;
                    
                                                return businessNetworkConnection
                                                    .submitTransaction(confirmLeaseTermination)
                                                    .should.be.rejectedWith('This action'+
                                                        ' needs to be taken by the lessee');
                                            });
                                            
                                    }); 
                            })
                        
                });

            

        });

       it('should not allow a lesee to cancel a lease contract and confirm the'+
        ' lease cancelation', async () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();


            const createLease =
                factory.newTransaction(namespace,CreateLease);
            createLease.startDate = new Date();
            createLease.endDate =
                new Date(
                    createLease.startDate.getFullYear()+5,
                    createLease.startDate.getMonth(),
                    createLease.startDate.getDay(),
                    createLease.startDate.getHours(),
                    createLease.startDate.getMinutes(),
                    createLease.startDate.getSeconds(),
                    createLease.startDate.getMilliseconds());
            createLease.vin = 'V987654321';
            createLease.make = 'Porsche';
            createLease.registrationNumber = '00-AA-00';
            createLease.totalValue= 75000;
            createLease.lessee =
                factory.newRelationship(namespace,Person,lessee.$identifier);

            await Utils.useIdentity(
                businessNetworkConnection,
                Person,
                companyOwner.$identifier,
                () => {
                    return businessNetworkConnection
                        .submitTransaction(createLease);
            });

            const confirmLease = factory.newTransaction(namespace,ConfirmLease);
            confirmLease.vin =  'V987654321';
            confirmLease.make = 'Porsche';
            confirmLease.registrationNumber = '00-AA-00';
            confirmLease.totalValue = 75000;
            confirmLease.lessee =
                factory.newRelationship(namespace,Person,lessee.$identifier);
            confirmLease.lessor =
                factory.newRelationship(namespace,Company,owner.$identifier);

            const terminateLease =
                factory.newTransaction(namespace,CancelLease);
            terminateLease.vin = confirmLease.vin ;
            terminateLease.make = confirmLease.make ;
            terminateLease.registrationNumber = confirmLease.registrationNumber;
            terminateLease.totalValue = confirmLease.totalValue;

            terminateLease.lessee = confirmLease.lessee;
            terminateLease.lessor = confirmLease.lessor;

            await businessNetworkConnection
                .connect('admin@bcar-network')
                .then( async () => {
                    return Utils
                        .useIdentity(
                            businessNetworkConnection,
                            Person,
                            lessee.$identifier, () => {
                                return businessNetworkConnection
                                    .submitTransaction(confirmLease);
                            });
                });

            return businessNetworkConnection
                .connect('admin@bcar-network')
                .then( () => {
                    Utils
                    .useIdentity(
                        businessNetworkConnection,
                        Person,
                        lessee.$identifier
                        ,() => {
                            return businessNetworkConnection
                                .submitTransaction(terminateLease)
                                .then(() => {
                                    return Utils
                                        .getVehicle(
                                            businessNetworkConnection,
                                            terminateLease.vin)
                                        .then( result => {
                                            assert.notEqual(
                                                result.lease,
                                                undefined);
                                            assert.equal(
                                                result.lease.state,
                                                WAITINGCANCELATION);
                                        });
                                })
                                .then(() => {
                                    // Issue Transaction for
                                    // confirming termination
                                    const confirmLeaseTermination =
                                        factory
                                        .newTransaction(
                                            namespace,ConfirmLeaseTermination);
                                    confirmLeaseTermination.vin = vehicle.vin;
                                    confirmLeaseTermination.make = vehicle.make;
                                    confirmLeaseTermination.registrationNumber =
                                        vehicle.registrationNumber;

                                    return businessNetworkConnection
                                        .submitTransaction(
                                            confirmLeaseTermination)
                                        .should.be.rejectedWith('This action'+
                                            ' needs to be taken by the lessor');
                                });

                        });
                });
        });

       it('should not create a Lease termination request transaction if the' +
        ' vehicle does not have a lease associated with',
            async () =>{
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();
        
                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = 'V987654321';
                terminateLease.make = 'Porsche';
                terminateLease.registrationNumber = '00-AA-00';
                terminateLease.totalValue = 75000;
    
                terminateLease.lessee =  factory.newRelationship(namespace,Person,lessee.$identifier);
                terminateLease.lessor = factory.newRelationship(namespace,Company,owner.$identifier);
    
                return Utils
                    .useIdentity(
                        businessNetworkConnection,
                        Person,
                        companyOwner.$identifier
                        ,() => {
                            return businessNetworkConnection
                                .submitTransaction(terminateLease)
                                .should.eventually.be
                                .rejectedWith(
                                    'Vehicle does not have any active lease.');
                    });
            });

        it('should not create a Lease termination confirmation transaction if' +
            ' the vehicle does not have terminate lease transaction pending',
            () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();

                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);

                return Utils.useIdentity(
                    businessNetworkConnection,
                    Person,
                    companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease)
                            .then( () => {
                                // Issue Transaction for confirming termination                              
                                
                                
                                const confirmLeaseTermination =
                                    factory
                                    .newTransaction(
                                        namespace,ConfirmLeaseTermination);
                                confirmLeaseTermination.vin = vehicle.vin;
                                confirmLeaseTermination.make = vehicle.make;
                                confirmLeaseTermination.registrationNumber =
                                    vehicle.registrationNumber;

                                return businessNetworkConnection
                                    .submitTransaction(
                                        confirmLeaseTermination)
                                    .should.eventually.be
                                    .rejectedWith(
                                        'A Cancel Lease Transaction needs to ' +
                                        'be issued first.');
                            });
                });
            });
        it('should allow a lessee to cancel a lease termination request if he made one', () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();

                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);

                const confirmLease = factory.newTransaction(namespace,ConfirmLease);
                confirmLease.vin =  'V987654321';
                confirmLease.make = 'Porsche';
                confirmLease.registrationNumber = '00-AA-00';
                confirmLease.totalValue = 75000;
                confirmLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);
                confirmLease.lessor =
                    factory.newRelationship(namespace,Company,owner.$identifier);
    
                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = confirmLease.vin ;
                terminateLease.make = confirmLease.make ;
                terminateLease.registrationNumber = confirmLease.registrationNumber;
                terminateLease.totalValue = confirmLease.totalValue;
    
                terminateLease.lessee = confirmLease.lessee;
                terminateLease.lessor = confirmLease.lessor;

                const cancelLeaseTermination =
                    factory
                    .newTransaction(
                        namespace,CancelLeaseTermination);
                cancelLeaseTermination.vin = vehicle.vin;
                cancelLeaseTermination.make = vehicle.make;
                cancelLeaseTermination.registrationNumber =
                    vehicle.registrationNumber;

                return Utils.usePersonIdentity(
                    businessNetworkConnection,
                    companyOwner.$identifier, () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease)
                            .then(() => {
                                return businessNetworkConnection
                                    .connect('admin@bcar-network')
                                    .then( () => {
                                        return Utils.usePersonIdentity(
                                            businessNetworkConnection,
                                            lessee.$identifier, () => {
                                               return businessNetworkConnection
                                                    .submitTransaction(confirmLease)
                                                    .then( () => {
                                                        return businessNetworkConnection
                                                            .submitTransaction(terminateLease)
                                                            .then( () => {
                                                                // Issue Transaction for canceling  termination
                                                                return businessNetworkConnection
                                                                .submitTransaction(
                                                                    cancelLeaseTermination)
                                                                .then( () => {
                                                                    return Utils
                                                                        .getVehicle(
                                                                            businessNetworkConnection,
                                                                            terminateLease.vin)
                                                                        .then( result => {
                                                                            assert.notEqual(
                                                                                result.lease,
                                                                                undefined);
                                                                            assert.equal(
                                                                                result.lease.state,
                                                                                VALID);
                                                                        });
                                                                } );
                                                            });
                                                    } );
                                            })
                                    });
                            });
                    });

        });
        it('should not allow a lessee to cancel a lease termination request if no request was made', () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();

                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);

                const cancelLeaseTermination =
                    factory
                    .newTransaction(
                        namespace,CancelLeaseTermination);
                cancelLeaseTermination.vin = vehicle.vin;
                cancelLeaseTermination.make = vehicle.make;
                cancelLeaseTermination.registrationNumber =
                    vehicle.registrationNumber;

                return Utils.usePersonIdentity(
                    businessNetworkConnection,
                    companyOwner.$identifier, () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease)
                            .then(() => {
                                return businessNetworkConnection
                                    .connect('admin@bcar-network')
                                    .then( () => {
                                        return Utils.usePersonIdentity(
                                            businessNetworkConnection,
                                            lessee.$identifier, () => {
                                               return businessNetworkConnection
                                                    .submitTransaction(
                                                        cancelLeaseTermination)
                                                    .should.eventually.be
                                                    .rejectedWith('A lease termination operation must be issued first.')
                                            })
                                    });
                            });
                    });

        });
        it('should allow a lessor to cancel a lease termination request if he made one',  () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();

                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);

                const confirmLease = factory.newTransaction(namespace,ConfirmLease);
                confirmLease.vin =  'V987654321';
                confirmLease.make = 'Porsche';
                confirmLease.registrationNumber = '00-AA-00';
                confirmLease.totalValue = 75000;
                confirmLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);
                confirmLease.lessor =
                    factory.newRelationship(namespace,Company,owner.$identifier);
    
                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = confirmLease.vin ;
                terminateLease.make = confirmLease.make ;
                terminateLease.registrationNumber = confirmLease.registrationNumber;
                terminateLease.totalValue = confirmLease.totalValue;
    
                terminateLease.lessee = confirmLease.lessee;
                terminateLease.lessor = confirmLease.lessor;

                const cancelLeaseTermination =
                    factory
                    .newTransaction(
                        namespace,CancelLeaseTermination);
                cancelLeaseTermination.vin = vehicle.vin;
                cancelLeaseTermination.make = vehicle.make;
                cancelLeaseTermination.registrationNumber =
                    vehicle.registrationNumber;

                return Utils.usePersonIdentity(
                    businessNetworkConnection,
                    companyOwner.$identifier, () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease)
                            .then(() => {
                               return businessNetworkConnection
                                    .connect('admin@bcar-network')
                                    .then( () => {
                                        return Utils.usePersonIdentity(
                                            businessNetworkConnection,
                                            lessee.$identifier, () => {
                                                return businessNetworkConnection
                                                    .submitTransaction(confirmLease);
                                            });
                                    });
                            }).then( () => {
                                return businessNetworkConnection
                                    .submitTransaction(terminateLease)
                                    .then( () => {
                                        // Issue Transaction for canceling  termination
                                        return businessNetworkConnection
                                        .submitTransaction(
                                            cancelLeaseTermination)
                                        .then(() => {
                                            return Utils
                                                .getVehicle(
                                                    businessNetworkConnection,
                                                    terminateLease.vin)
                                                .then( result => {
                                                    assert.notEqual(
                                                        result.lease,
                                                        undefined);
                                                    assert.equal(
                                                        result.lease.state,
                                                        VALID);
                                                });
                                        })

                                    });
                            });
                    });
        });
        it('should not allow a lessor to cancel a lease termination request if no request was made', () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();

                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory.newRelationship(namespace,Person,lessee.$identifier);

                const cancelLeaseTermination =
                    factory
                    .newTransaction(
                        namespace,CancelLeaseTermination);
                cancelLeaseTermination.vin = vehicle.vin;
                cancelLeaseTermination.make = vehicle.make;
                cancelLeaseTermination.registrationNumber =
                    vehicle.registrationNumber;

                return Utils.usePersonIdentity(
                    businessNetworkConnection,
                    companyOwner.$identifier, () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease)
                            .then(() => {
                               return businessNetworkConnection
                                    .submitTransaction(
                                        cancelLeaseTermination)
                                    .should.eventually.be
                                    .rejectedWith('A lease termination operation must be issued first.');
                            });
                    });
        });

        it('should allow a judicial officer to revoke a vehicle lease', async () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();
                const lesseeCompany =
                    factory.newResource(namespace, Company, '968364154');
                lesseeCompany.name = "Just Do IT";
                lesseeCompany.address = 'Republica 20 - Lisboa';

                const companyOwnerRelation =
                    factory
                    .newRelationship(namespace,Person, lessee.$identifier);
                var owners = new Array();
                owners.push(companyOwnerRelation);
                lesseeCompany.owners = owners;

                var judicialOfficer = factory.newResource(namespace, JudicialOfficer, '123456789');
                judicialOfficer.name = 'Jonh';
                judicialOfficer.surname = 'Dough';
                judicialOfficer.address = 'Austria';
                judicialOfficer.court = "Innsbruck";

                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        lesseeCompany.$identifier);

                await Utils.addParticipant(
                    businessNetworkConnection,
                    JudicialOfficer,
                    judicialOfficer);

                await Utils.addParticipant(
                    businessNetworkConnection,
                    Company,
                    lesseeCompany);

                await Utils.useIdentity(
                    businessNetworkConnection,
                    Person,
                    companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease);
                });

                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = 'V987654321';
                terminateLease.make = 'Porsche';
                terminateLease.registrationNumber = '00-AA-00';
                terminateLease.totalValue = 75000;

                terminateLease.lessee =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        lesseeCompany.$identifier);

                terminateLease.lessor =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        owner.$identifier);

                return businessNetworkConnection
                    .connect('admin@bcar-network')
                    .then( () => {
                        return  Utils
                            .useIdentity(
                                businessNetworkConnection,
                                JudicialOfficer,
                                judicialOfficer.$identifier,
                                () => {
                                    return businessNetworkConnection
                                        .submitTransaction(terminateLease)
                                        .then(() => {
                                            return Utils
                                                .getVehicle(
                                                    businessNetworkConnection,
                                                    terminateLease.vin)
                                                .then( result => {
                                                    assert.equal(
                                                        result.lease,
                                                        undefined);
                                                });
                                        })

                                });
                    });
        });
        it('should not allow a non Owner nor a lessee nor a judicial officer to cancel a lease termination request',
            async () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();
                const lesseeCompany =
                    factory.newResource(namespace, Company, '968364154');
                lesseeCompany.name = "Just Do IT";
                lesseeCompany.address = 'Republica 20 - Lisboa';

                const companyOwnerRelation =
                    factory
                    .newRelationship(namespace,Person, lessee.$identifier);
                var owners = new Array();
                owners.push(companyOwnerRelation);
                lesseeCompany.owners = owners;


                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        lesseeCompany.$identifier);

                var unauthorizedPerson =
                    factory
                    .newResource(namespace, Person, '777888666');

                unauthorizedPerson.name = 'Richard';
                unauthorizedPerson.surname = 'Stallman'
                unauthorizedPerson.address = 'USA';

                await Utils.addParticipant(
                    businessNetworkConnection,
                    Person,
                    unauthorizedPerson);

                await Utils.addParticipant(
                    businessNetworkConnection,
                    Company,
                    lesseeCompany);

                await Utils.useIdentity(
                    businessNetworkConnection,
                    Person,
                    companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease);
                });

                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = 'V987654321';
                terminateLease.make = 'Porsche';
                terminateLease.registrationNumber = '00-AA-00';
                terminateLease.totalValue = 75000;

                terminateLease.lessee =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        lesseeCompany.$identifier);

                terminateLease.lessor =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        owner.$identifier);

                return businessNetworkConnection
                    .connect('admin@bcar-network')
                    .then( () => {
                        return  Utils
                            .useIdentity(
                                businessNetworkConnection,
                                Person,
                                lessee.$identifier,
                                () => {
                                    return businessNetworkConnection
                                        .submitTransaction(terminateLease);
                                });
                    })
                    .then(() => {
                        return businessNetworkConnection
                            .connect('admin@bcar-network')
                            .then( () => {
                                return Utils.usePersonIdentity(
                                    businessNetworkConnection,
                                    unauthorizedPerson.$identifier,
                                    () => {
                                        // Issue Transaction for confirming
                                        // termination
                                        const cancelLeaseTermination =
                                            factory
                                            .newTransaction(
                                                namespace,CancelLeaseTermination);
                                        cancelLeaseTermination.vin = vehicle.vin;
                                        cancelLeaseTermination.make = vehicle.make;
                                        cancelLeaseTermination.registrationNumber =
                                            vehicle.registrationNumber;


                                            return businessNetworkConnection
                                                .submitTransaction(
                                                    cancelLeaseTermination)
                                                .should.eventually.be
                                                .rejectedWith(
                                                    'Only the owner or the' +
                                                    ' lessee can confirm a lease termination');
                                        });
                            });

                    });
        });
        it('should not allow a non Owner nor a lessee nor a judicial officer to confirm a lease termination request',
            async () => {
                const factory =
                    businessNetworkConnection.getBusinessNetwork().getFactory();
                const lesseeCompany =
                    factory.newResource(namespace, Company, '968364154');
                lesseeCompany.name = "Just Do IT";
                lesseeCompany.address = 'Republica 20 - Lisboa';

                const companyOwnerRelation =
                    factory
                    .newRelationship(namespace,Person, lessee.$identifier);
                var owners = new Array();
                owners.push(companyOwnerRelation);
                lesseeCompany.owners = owners;


                const createLease =
                    factory.newTransaction(namespace,CreateLease);
                createLease.startDate = new Date();
                createLease.endDate =
                    new Date(
                        createLease.startDate.getFullYear()+5,
                        createLease.startDate.getMonth(),
                        createLease.startDate.getDay(),
                        createLease.startDate.getHours(),
                        createLease.startDate.getMinutes(),
                        createLease.startDate.getSeconds(),
                        createLease.startDate.getMilliseconds());
                createLease.vin = 'V987654321';
                createLease.make = 'Porsche';
                createLease.registrationNumber = '00-AA-00';
                createLease.totalValue= 75000;
                createLease.lessee =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        lesseeCompany.$identifier);

                var unauthorizedPerson =
                    factory
                    .newResource(namespace, Person, '777888666');

                unauthorizedPerson.name = 'Richard';
                unauthorizedPerson.surname = 'Stallman'
                unauthorizedPerson.address = 'USA';

                await Utils.addParticipant(
                    businessNetworkConnection,
                    Person,
                    unauthorizedPerson);

                await Utils.addParticipant(
                    businessNetworkConnection,
                    Company,
                    lesseeCompany);

                await Utils.useIdentity(
                    businessNetworkConnection,
                    Person,
                    companyOwner.$identifier,
                    () => {
                        return businessNetworkConnection
                            .submitTransaction(createLease);
                });

                const terminateLease =
                    factory.newTransaction(namespace,CancelLease);
                terminateLease.vin = 'V987654321';
                terminateLease.make = 'Porsche';
                terminateLease.registrationNumber = '00-AA-00';
                terminateLease.totalValue = 75000;

                terminateLease.lessee =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        lesseeCompany.$identifier);

                terminateLease.lessor =
                    factory
                    .newRelationship(
                        namespace,
                        Company,
                        owner.$identifier);

                return businessNetworkConnection
                    .connect('admin@bcar-network')
                    .then( () => {
                        return  Utils
                            .useIdentity(
                                businessNetworkConnection,
                                Person,
                                lessee.$identifier,
                                () => {
                                    return businessNetworkConnection
                                        .submitTransaction(terminateLease);
                                });
                    })
                    .then(() => {
                        return businessNetworkConnection
                            .connect('admin@bcar-network')
                            .then( () => {
                                return Utils
                                    .usePersonIdentity(
                                        businessNetworkConnection,
                                        unauthorizedPerson.$identifier,
                                        () => {
                                        // Issue Transaction for confirming
                                        // termination
                                            const confirmLeaseTermination =
                                                factory
                                                .newTransaction(
                                                    namespace,
                                                    ConfirmLeaseTermination);
                                            confirmLeaseTermination.vin =
                                                vehicle.vin;
                                            confirmLeaseTermination.make =
                                                vehicle.make;
                                            confirmLeaseTermination
                                                .registrationNumber =
                                                    vehicle.registrationNumber;

                                            return businessNetworkConnection
                                                .submitTransaction(
                                                    confirmLeaseTermination)
                                                .should.eventually.be
                                                .rejectedWith(
                                                    'Only the owner or the' +
                                                    ' lessee can confirm a lease termination');
                                                });
                                        });
                            });

                    });
    });
});
