'use strict';
/**
 * Write the unit tests for your transction processor functions here
 */

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection =
    require('composer-client').BusinessNetworkConnection;
const IdCard = require('composer-common').IdCard;
const MemoryCardStore = require('composer-common').MemoryCardStore;

const chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var assert = require('assert');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const namespace = 'pt.irn.bcar';
const assetType = 'Vehicle';
const Person = 'Person';
const Judge = 'Judge';
const Inspector = 'Inspector';
const VALID = 'VALID';
const WAITING = 'WAITING';
const Ownership = 'Ownership';
const ConfirmOwnership = 'ConfirmOwnership';
const CreateVehicle = 'CreateVehicle';
const ChangeOwner = 'ChangeOwner';
const STATE_ACTIVE = 'ACTIVE';


// Required to execute (before) setup function when the test starts
var Utils = require('./before.js');

describe('#' + namespace, () => {
  let businessNetworkConnection;
  let owner;
  let sampleVehicle;

  beforeEach(() => {
      businessNetworkConnection =
        new BusinessNetworkConnection({cardStore: Utils.cardStore});

      return Utils.setup(businessNetworkConnection)
          .then(() => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            owner = factory.newResource(namespace, Person, '222333222');
            owner.surname = 'Santos';
            owner.name = 'José';
            owner.address = 'Liberdade 21';

            const ownerRelation =
                factory.newRelationship(namespace, Person, owner.$identifier);

            sampleVehicle =
                factory.newResource(namespace, 'Vehicle', 'V123456789');
            sampleVehicle.vin = sampleVehicle.$identifier;
            sampleVehicle.registrationNumber = '01-AA-00';
            sampleVehicle.make = 'Opel';
            sampleVehicle.typeVersion = 'Corsa';
            sampleVehicle.category = 'M';
            sampleVehicle.vClass = 'LIGHT';
            sampleVehicle.state = STATE_ACTIVE;


            const engineS = factory.newConcept(namespace, 'Engine');
            engineS.capacity = 0;
            engineS.fuelType = 'GASOLINE';
            engineS.ratedSpeed = 0;
            engineS.ein = 'E11';

            sampleVehicle.engine = engineS;
            sampleVehicle.maxLadenMass = 3500;

            const ownership = factory.newConcept(namespace, Ownership);
            const owners = new Array();
            owners.push(ownership);
            ownership.owner = ownerRelation;
            ownership.share = 1;
            ownership.state = VALID;

            sampleVehicle.owners = owners;
            sampleVehicle.certificateHolder = ownerRelation;

            const emissionsSample = factory.newConcept(namespace, 'Emissions');
            emissionsSample.emCO = 10;
            emissionsSample.emHC = 10;
            emissionsSample.emNOx = 10;
            emissionsSample.emHCNOx = 10;
            emissionsSample.emCO2 = 10;
            emissionsSample.emFuelConsumption = 7.5;

            sampleVehicle.emissions = emissionsSample;

            //Create vehicle and insert it in the blockchain
            const vehicle =
                factory.newResource(namespace, assetType, 'V987654321');

            vehicle.registrationNumber = '00-AA-00';
            vehicle.make = 'Porsche';
            vehicle.typeVersion = '911';
            vehicle.owners = owners;
            vehicle.certificateHolder = ownerRelation;
            vehicle.category = 'M';
            vehicle.vClass = 'LIGHT';
            vehicle.maxLadenMass = 3500;
            const engine = factory.newConcept(namespace, 'Engine');
            engine.capacity = 0;
            engine.fuelType = 'GASOLINE';
            engine.ratedSpeed = 0;
            engine.ein = 'E10';
            vehicle.engine = engine;

            const emissions = factory.newConcept(namespace, 'Emissions');
            emissions.emCO = 10;
            emissions.emHC = 10;
            emissions.emNOx = 10;
            emissions.emHCNOx = 10;
            emissions.emCO2 = 10;
            emissions.emFuelConsumption = 7.5;

            vehicle.emissions = emissions;
            vehicle.state = STATE_ACTIVE;

            //Adding owner to registry
            Utils.addParticipant(businessNetworkConnection,Person,owner);

            return businessNetworkConnection
                .getAssetRegistry(namespace + '.' + assetType)
                .then(registry => {
                    return registry.add(vehicle);
                  });
          });
    });

  describe('CreateVehicle()', () => {
    it('should create a vehicle', () => {
          const factory =
              businessNetworkConnection.getBusinessNetwork().getFactory();

          const createVehicle =
              factory.newTransaction(namespace, CreateVehicle);
          createVehicle.vehicle = sampleVehicle;
          console.log(sampleVehicle);
          return businessNetworkConnection
                .submitTransaction(createVehicle)
                .then(() => {
                    return Utils.getVehicle(businessNetworkConnection,sampleVehicle.vin)
                        .then(result => {

                            assert.equal(
                                result.registrationNumber,
                                sampleVehicle.registrationNumber);
                            assert.equal(
                                result.make,
                                createVehicle.vehicle.make);
                            assert.equal(
                                result.typeVersion,
                                sampleVehicle.typeVersion);
                            assert.equal(
                                result.owners[0].owner.$identifier,
                                owner.$identifier);
                            assert.equal(
                                result.certificateHolder.$identifier,
                                owner.$identifier);
                        });
                });
        });

    it('should not create a vehicle with the same vin', () => {
            const factory =
              businessNetworkConnection.getBusinessNetwork().getFactory();

            const vehicle =
            factory.newResource(namespace, 'Vehicle', 'V987654321');
            vehicle.vin = vehicle.$identifier;
            vehicle.registrationNumber = '01-AA-00';
            vehicle.make = 'Opel';
            vehicle.typeVersion = 'Corsa';
            vehicle.category = 'M';
            vehicle.vClass = 'LIGHT';
            const engine = factory.newConcept(namespace, 'Engine');
            engine.capacity = 0;
            engine.fuelType = 'GASOLINE';
            engine.ratedSpeed = 0;
            engine.ein = 'E11';
            vehicle.engine = engine;

            vehicle.maxLadenMass = 3500;
            const emissions = factory.newConcept(namespace, 'Emissions');
            emissions.emCO = 10;
            emissions.emHC = 10;
            emissions.emNOx = 10;
            emissions.emHCNOx = 10;
            emissions.emCO2 = 10;
            emissions.emFuelConsumption = 7.5;

            vehicle.emissions = emissions;

            const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
            const ownership =
                factory.newConcept(namespace, Ownership);
            ownership.owner = ownerRelation;
            ownership.share = 1;
            ownership.state = VALID;
            const owners = new Array();
            owners.push(ownership);

            vehicle.owners = owners;
            vehicle.certificateHolder = ownerRelation;
            vehicle.state = STATE_ACTIVE;

            const createVehicle =
            factory.newTransaction(namespace, CreateVehicle);
            createVehicle.vehicle = vehicle;

            return businessNetworkConnection
              .submitTransaction(createVehicle)
              .should.eventually.be.rejectedWith(
                  'Vehicle with same vin or ein or ' +
                  'registration number already exists.');
        });

    it('should not create a vehicle with the same ein', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E10';
        sampleVehicle.engine = engine;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = sampleVehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
                .should.eventually.be.rejectedWith(
                  'Vehicle with same vin or ein or ' +
                  'registration number already exists.');
      });

    it('should not create a vehicle with the same registration Number',
      () => {
          const factory =
              businessNetworkConnection.getBusinessNetwork().getFactory();

          sampleVehicle.registrationNumber = '00-AA-00';

          const createVehicle =
              factory.newTransaction(namespace, CreateVehicle);
          createVehicle.vehicle = sampleVehicle;

          return businessNetworkConnection
              .submitTransaction(createVehicle)
              .should.eventually.be.rejectedWith(
                  'Vehicle with same vin or ein or ' +
                  'registration number already exists.');
        });

    it('should create a vehicle type M with a vehicle class (light)', () => {
        const factory =
            businessNetworkConnection.getBusinessNetwork().getFactory();

        sampleVehicle.vClass = 'LIGHT';

        const createVehicle =
            factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = sampleVehicle;

        return businessNetworkConnection
            .submitTransaction(createVehicle)
            .then(() => {
                return Utils.getVehicle(businessNetworkConnection,sampleVehicle.vin)
                    .then(result => {

                        assert.equal(result.vin, sampleVehicle.vin);
                        assert.equal(result.make,sampleVehicle.make);
                        assert.equal(
                            result.typeVersion,
                            sampleVehicle.typeVersion);
                        assert.equal(
                            result.owners[0].owner.$identifier,
                            owner.$identifier);
                        assert.equal(
                            result.certificateHolder.$identifier,
                            owner.$identifier);
                        assert.equal(result.category,sampleVehicle.category);
                        assert.equal(result.vClass,sampleVehicle.vClass);
                      });

              });
      });

    it('should create a vehicle type N with a vehicle class (heavy)', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        sampleVehicle.category = 'N';
        sampleVehicle.vClass = 'HEAVY';

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = sampleVehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
            .then(() => {
                return Utils.getVehicle(businessNetworkConnection,sampleVehicle.vin)
                    .then(result => {
                        assert.equal(result.vin, sampleVehicle.vin);
                        assert.equal(result.make, sampleVehicle.make);
                        assert.equal(
                            result.typeVersion,
                            sampleVehicle.typeVersion);
                        assert.equal(
                          result.owners[0].owner.$identifier,
                          owner.$identifier);
                        assert.equal(
                          result.certificateHolder.$identifier,
                          owner.$identifier);
                        assert.equal(result.category, sampleVehicle.category);
                        assert.equal(result.vClass, sampleVehicle.vClass);
                    });
            });
      });

    it('should not create a vehicle type M without a vehicle class', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();
        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Opel';
        vehicle.typeVersion = 'Corsa';
        vehicle.category = 'M';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.maxLadenMass = 3500;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;
        vehicle.state = STATE_ACTIVE;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection
              .submitTransaction(createVehicle)
              .should.eventually.be.rejectedWith(
                  'Vehicle from categories M or N' +
                  ' need to have a vehicle class.');
      });

    it('should not create a vehicle type N without a vehicle class', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();
        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Opel';
        vehicle.typeVersion = 'Corsa';
        vehicle.category = 'N';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.state = STATE_ACTIVE;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        vehicle.maxLadenMass = 3500;
        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection
              .submitTransaction(createVehicle)
              .should.eventually.be.rejectedWith(
                  'Vehicle from categories M or N ' +
                  'need to have a vehicle class.');
      });

    it('should not create a vehicle type L or T with a vehicle class', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Suzuki';
        vehicle.typeVersion = 'Hayabusa';
        vehicle.category = 'L';
        vehicle.vClass = 'HEAVY';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.pwRatio = 2;
        vehicle.state = STATE_ACTIVE;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection
              .submitTransaction(createVehicle)
              .should.eventually.be.rejectedWith(
                  'Vehicle from categories L or T ' +
                  'can not have a vehicle class.');
      });

    it('should create a vehicle type L with pwRatio', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Suzuki';
        vehicle.typeVersion = 'Hayabusa';
        vehicle.category = 'L';
        vehicle.pwRatio = 2;
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.state = STATE_ACTIVE;
        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
            .then(() => {
            return Utils.getVehicle(businessNetworkConnection,vehicle.vin)
                .then(result => {

                    assert.equal(result.vin,createVehicle.vehicle.vin);
                    assert.equal(result.make,createVehicle.vehicle.make);
                    assert.equal(
                        result.typeVersion,
                        createVehicle.vehicle.typeVersion);
                    assert.equal(
                        result.owners[0].owner.$identifier,
                        owner.$identifier);
                    assert.equal(
                        result.certificateHolder.$identifier,
                        owner.$identifier);
                    assert.equal(result.category,vehicle.category);
                    assert.equal(result.pwRatio,vehicle.pwRatio);
                });
        });
      });

    it('should not create a vehicle type L without pwRatio', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Suzuki';
        vehicle.typeVersion = 'Hayabusa';
        vehicle.category = 'L';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.state = STATE_ACTIVE;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection
              .submitTransaction(createVehicle)
              .should.eventually.be.rejectedWith(
                  'Vehicle from category L need to have pwRatio.');
      });

    it('should not create a vehicle with type M with pwRatio', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        sampleVehicle.pwRatio = 2;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = sampleVehicle;

        return businessNetworkConnection
              .submitTransaction(createVehicle)
              .should.eventually.be.rejectedWith(
                  'Vehicle from categories M, N or T can not have pwRatio.');
      });

    it('should not create a vehicle with type N with pwRatio', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Scania';
        vehicle.typeVersion = 'S730';
        vehicle.category = 'N';
        vehicle.vClass = 'HEAVY';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.pwRatio = 2;
        vehicle.maxLadenMass = 5000;
        vehicle.state = STATE_ACTIVE;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection
              .submitTransaction(createVehicle)
              .should.eventually.be.rejectedWith(
                  'Vehicle from categories M, N or T can not have pwRatio.');
      });

    it('should not create a vehicle with type T with pwRatio', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Lamborghini';
        vehicle.typeVersion = 'Mach VRT';
        vehicle.category = 'T';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.maxLadenMass = 3500;
        vehicle.pwRatio = 2;
        vehicle.state = STATE_ACTIVE;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
                .should.eventually.be.rejectedWith(
                  'Vehicle from categories M, N or T can not have pwRatio.');
      });

    it('should create a vehicle of category M with maxLadenMass', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = sampleVehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
            .then(() => {
            return Utils.getVehicle(businessNetworkConnection,sampleVehicle.vin)
                .then(result => {
                    assert.equal(result.vin, sampleVehicle.vin);
                    assert.equal(result.make, sampleVehicle.make);
                    assert.equal(result.typeVersion,sampleVehicle.typeVersion);
                    assert.equal(
                      result.owners[0].owner.$identifier,
                      owner.$identifier);
                    assert.equal(
                      result.certificateHolder.$identifier,
                      owner.$identifier);
                    assert.equal(result.category,sampleVehicle.category);
                    assert.equal(result.vClass,sampleVehicle.vClass);
                    assert.equal(
                      result.maxLadenMass,
                      sampleVehicle.maxLadenMass);
                });
          });
      });

    it('should create a vehicle of category N with maxLadenMass', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        sampleVehicle.category = 'N';
        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = sampleVehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
            .then(() => {
                return Utils.getVehicle(businessNetworkConnection,sampleVehicle.vin)
                    .then(result => {

                        assert.equal(result.vin, sampleVehicle.vin);
                        assert.equal(result.make, sampleVehicle.make);
                        assert.equal(
                          result.typeVersion,
                          sampleVehicle.typeVersion);
                        assert.equal(
                          result.owners[0].owner.$identifier,
                          owner.$identifier);
                        assert.equal(
                          result.certificateHolder.$identifier,
                          owner.$identifier);
                        assert.equal(result.category,sampleVehicle.category);
                        assert.equal(result.vClass,sampleVehicle.vClass);
                        assert.equal(
                          result.maxLadenMass,
                          sampleVehicle.maxLadenMass);
                    });
          });
      });

    it('should create a vehicle of category T with maxLadenMass', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        sampleVehicle.category = 'T';
        sampleVehicle.vClass = undefined;
        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = sampleVehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
            .then(() => {
            return Utils.getVehicle(businessNetworkConnection,sampleVehicle.vin)
                  .then(result => {

                      assert.equal(result.vin, sampleVehicle.vin);
                      assert.equal(result.make,sampleVehicle.make);
                      assert.equal(
                          result.typeVersion,
                          sampleVehicle.typeVersion);
                      assert.equal(
                          result.owners[0].owner.$identifier,
                          owner.$identifier);
                      assert.equal(
                          result.certificateHolder.$identifier,
                          owner.$identifier);
                      assert.equal(result.category,sampleVehicle.category);
                      assert.equal(
                          result.maxLadenMass,
                          sampleVehicle.maxLadenMass);
                    });
          });
      });

    it('should not create a vehicle of type M without maxLadenMass', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Opel';
        vehicle.typeVersion = 'Corsa';
        vehicle.category = 'M';
        vehicle.vClass = 'LIGHT';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.state = STATE_ACTIVE;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
                .should.eventually.be.rejectedWith(
                      'Vehicle from categories M, N or T need to have' +
                      ' maxLadenMass.');
      });

    it('should not create a vehicle of type N without maxLadenMass', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Scania';
        vehicle.typeVersion = 'S730';
        vehicle.category = 'N';
        vehicle.vClass = 'HEAVY';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.state = STATE_ACTIVE;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
                .should.eventually.be.rejectedWith(
                  'Vehicle from categories M, N or T ' +
                  'need to have maxLadenMass.');
      });

    it('should not create a vehicle of type T without maxLadenMass', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Lamborghini';
        vehicle.typeVersion = 'Mach VRT';
        vehicle.category = 'T';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.state = STATE_ACTIVE;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
                .should.eventually.be.rejectedWith(
                  'Vehicle from categories M, N or T need ' +
                  'to have maxLadenMass.');
      });

    it('should not create a vehicle of type L with maxLadenMass', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        const vehicle =
          factory.newResource(namespace, 'Vehicle', 'V123456789');
        vehicle.vin = vehicle.$identifier;
        vehicle.registrationNumber = '01-AA-00';
        vehicle.make = 'Suzuki';
        vehicle.typeVersion = 'Hayabusa';
        vehicle.category = 'L';
        const engine = factory.newConcept(namespace, 'Engine');
        engine.capacity = 0;
        engine.fuelType = 'GASOLINE';
        engine.ratedSpeed = 0;
        engine.ein = 'E11';
        vehicle.engine = engine;
        vehicle.pwRatio = 2;
        vehicle.maxLadenMass = 10;
        vehicle.state = STATE_ACTIVE;

        const emissions = factory.newConcept(namespace, 'Emissions');
        emissions.emCO = 10;
        emissions.emHC = 10;
        emissions.emNOx = 10;
        emissions.emHCNOx = 10;
        emissions.emCO2 = 10;
        emissions.emFuelConsumption = 7.5;

        vehicle.emissions = emissions;

        const ownerRelation =
              factory.newRelationship(namespace, Person, owner.$identifier);
        const ownership =
            factory.newConcept(namespace, Ownership);
        ownership.owner = ownerRelation;
        ownership.share = 1;
        ownership.state = VALID;
        const owners = new Array();
        owners.push(ownership);
        vehicle.owners = owners;
        vehicle.certificateHolder = ownerRelation;

        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = vehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
                .should.eventually.be.rejectedWith(
                  'Vehicle from category L can not have maxLadenMass.');
      });

    it('should create a diesel vehicle with emParticles and '
        + 'emAbsortionCoefficient', () => {
            const factory =
              businessNetworkConnection.getBusinessNetwork().getFactory();

            sampleVehicle.engine.fuelType = 'DIESEL';
            sampleVehicle.emissions.emParticles = 2;
            sampleVehicle.emissions.emAbsortionCoefficient = 2;
            sampleVehicle.state = STATE_ACTIVE;
            const createVehicle =
              factory.newTransaction(namespace, CreateVehicle);
            createVehicle.vehicle = sampleVehicle;

            return businessNetworkConnection.submitTransaction(createVehicle)
                .then(() => {
                    return Utils.getVehicle(businessNetworkConnection,sampleVehicle.vin)
                          .then(result => {

                            assert.equal(result.vin,sampleVehicle.vin);
                            assert.equal(result.make,sampleVehicle.make);
                            assert.equal(
                              result.typeVersion,
                              sampleVehicle.typeVersion);
                            assert.equal(
                              result.owners[0].owner.$identifier,
                              owner.$identifier);
                            assert.equal(
                              result.certificateHolder.$identifier,
                              owner.$identifier);
                            assert.equal(
                              result.category,
                              sampleVehicle.category);
                            assert.equal(result.vClass,sampleVehicle.vClass);
                            assert.equal(
                              result.emissions.emParticles,
                              sampleVehicle.emissions.emParticles);
                            assert.equal(
                              result.emissions.emAbsortionCoefficient,
                              sampleVehicle.emissions.emAbsortionCoefficient);
                          });
                });
          });

    it('should not create a diesel vehicle without emParticles and' +
      ' emAbsortionCoefficient', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        sampleVehicle.engine.fuelType = 'DIESEL';
        sampleVehicle.state = STATE_ACTIVE;
        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = sampleVehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
                .should.eventually.be.rejectedWith(
                  'Vehicle with DIESEL fuelType engines need to have ' +
                  'emParticles and emAbsortionCoefficient.');
      });

    it('should not create a non-diesel vehicle with emParticles and '
        + 'emAbsortionCoefficient', () => {
        const factory =
          businessNetworkConnection.getBusinessNetwork().getFactory();

        sampleVehicle.emissions.emParticles = 2;
        sampleVehicle.emissions.emAbsortionCoefficient = 2;
        sampleVehicle.state = STATE_ACTIVE;
        const createVehicle =
          factory.newTransaction(namespace, CreateVehicle);
        createVehicle.vehicle = sampleVehicle;

        return businessNetworkConnection.submitTransaction(createVehicle)
                .should.eventually.be.rejectedWith(
                  'Vehicle with non-DIESEL fuelType engines can not have ' +
                  'emParticles and emAbsortionCoefficient.');
      });

  });

});

