'use strict';
/**
 * Write the unit tests for your transction processor functions here
 */

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection =
    require('composer-client').BusinessNetworkConnection;
const BusinessNetworkDefinition =
    require('composer-common').BusinessNetworkDefinition;
const IdCard = require('composer-common').IdCard;
const MemoryCardStore = require('composer-common').MemoryCardStore;

const path = require('path');
const chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var assert = require('assert');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const namespace = 'pt.irn.bcar';
const assetType = 'Vehicle';
const Person = 'Person';
const Judge = 'Judge';
const Inspector = 'Inspector';
const VALID = 'VALID';
const WAITING = 'WAITING';
const Ownership = 'Ownership';
const ConfirmOwnership = 'ConfirmOwnership';
const CreateVehicle = 'CreateVehicle';
const ChangeOwner = 'ChangeOwner';


// Required to execute (before) setup function when the test starts
var Utils = require('./before.js');

describe('#' + namespace, () => {
  let businessNetworkConnection;
  let owner;
  let sampleVehicle;

  beforeEach(() => {
      businessNetworkConnection =
        new BusinessNetworkConnection({cardStore: Utils.cardStore});

      return Utils.setup(businessNetworkConnection)
          .then(() => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            owner = factory.newResource(namespace, Person, '222333222');
            owner.surname = 'Santos';
            owner.name = 'José';
            owner.address = 'Liberdade 21';

            //Adding owner to registry
            return Utils.addParticipant(businessNetworkConnection,Person,owner);

          });
    });

    describe('IssueIdentity()', () => {
        it('should issue an identity and be able to ping the system', () => {
            return Utils.usePersonIdentity(businessNetworkConnection,owner.fn,() => {
                return businessNetworkConnection.ping();
            });
        });
    });

});

