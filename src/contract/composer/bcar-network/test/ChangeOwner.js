'use strict';
/**
 * Write the unit tests for your transction processor functions here
 */

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection =
    require('composer-client').BusinessNetworkConnection;
const BusinessNetworkDefinition =
    require('composer-common').BusinessNetworkDefinition;
const IdCard = require('composer-common').IdCard;
const MemoryCardStore = require('composer-common').MemoryCardStore;

const path = require('path');
const chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var assert = require('assert');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const namespace = 'pt.irn.bcar';
const assetType = 'Vehicle';
const Person = 'Person';
const Judge = 'Judge';
const Inspector = 'Inspector';
const VALID = 'VALID';
const WAITING = 'WAITING';
const STATE_ACTIVE = 'ACTIVE';
const Ownership = 'Ownership';
const ConfirmOwnership = 'ConfirmOwnership';
const CreateVehicle = 'CreateVehicle';
const ChangeOwner = 'ChangeOwner';


// Required to execute (before) setup function when the test starts
var Utils = require('./before.js');

describe('#' + namespace, () => {

  let businessNetworkConnection;
  let owner;
  let sampleVehicle;

  beforeEach(() => {
      businessNetworkConnection =
        new BusinessNetworkConnection({cardStore: Utils.cardStore});

     return Utils.setup(businessNetworkConnection).then(() => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            owner = factory.newResource(namespace, Person, '222333222');
            owner.surname = 'Santos';
            owner.name = 'José';
            owner.address = 'Liberdade 21';

            const ownerRelation =
                factory.newRelationship(namespace, Person, owner.$identifier);

            sampleVehicle =
                factory.newResource(namespace, 'Vehicle', 'V123456789');
            sampleVehicle.vin = sampleVehicle.$identifier;
            sampleVehicle.registrationNumber = '01-AA-00';
            sampleVehicle.make = 'Opel';
            sampleVehicle.typeVersion = 'Corsa';
            sampleVehicle.category = 'M';
            sampleVehicle.vClass = 'LIGHT';

            const engineS = factory.newConcept(namespace, 'Engine');
            engineS.capacity = 0;
            engineS.fuelType = 'GASOLINE';
            engineS.ratedSpeed = 0;
            engineS.ein = 'E11';

            sampleVehicle.engine = engineS;
            sampleVehicle.maxLadenMass = 3500;

            const ownership = factory.newConcept(namespace, Ownership);
            const owners = new Array();
            owners.push(ownership);
            ownership.owner = ownerRelation;
            ownership.share = 1;
            ownership.state = VALID;

            sampleVehicle.owners = owners;
            sampleVehicle.certificateHolder = ownerRelation;

            const emissionsSample = factory.newConcept(namespace, 'Emissions');
            emissionsSample.emCO = 10;
            emissionsSample.emHC = 10;
            emissionsSample.emNOx = 10;
            emissionsSample.emHCNOx = 10;
            emissionsSample.emCO2 = 10;
            emissionsSample.emFuelConsumption = 7.5;

            sampleVehicle.emissions = emissionsSample;
            sampleVehicle.state = STATE_ACTIVE;
            //Create vehicle and insert it in the blockchain
            const vehicle =
                factory.newResource(namespace, assetType, 'V987654321');

            vehicle.registrationNumber = '00-AA-00';
            vehicle.make = 'Porsche';
            vehicle.typeVersion = '911';
            vehicle.owners = owners;
            vehicle.certificateHolder = ownerRelation;
            vehicle.category = 'M';
            vehicle.vClass = 'LIGHT';
            vehicle.maxLadenMass = 3500;
            const engine = factory.newConcept(namespace, 'Engine');
            engine.capacity = 0;
            engine.fuelType = 'GASOLINE';
            engine.ratedSpeed = 0;
            engine.ein = 'E10';
            vehicle.engine = engine;
            vehicle.state = STATE_ACTIVE;

            const emissions = factory.newConcept(namespace, 'Emissions');
            emissions.emCO = 10;
            emissions.emHC = 10;
            emissions.emNOx = 10;
            emissions.emHCNOx = 10;
            emissions.emCO2 = 10;
            emissions.emFuelConsumption = 7.5;

            vehicle.emissions = emissions;

            //Adding owner to registry
            Utils.addParticipant(businessNetworkConnection,Person,owner);

            return businessNetworkConnection
                .getAssetRegistry(namespace + '.' + assetType)
                .then(registry => {
                    return registry.add(vehicle);
                  });
          });
    });

 describe('ChangeOwner()', () => {
    it('should change the owner of a vehicle considering a single owner',
        () => {

            const factory =
                businessNetworkConnection
                .getBusinessNetwork()
                .getFactory();

            const ownerRelation =
                factory
                .newRelationship(
                    namespace,
                    Person,
                    owner.$identifier);


            const newOwner =
                factory
                .newResource(namespace,Person,'333333222');
            newOwner.name = "João";
            newOwner.surname = "Cruz";
            newOwner.address = "Lisb";

            return Utils.addParticipant(businessNetworkConnection,Person,newOwner)
                .then(() => {
                    return Utils.usePersonIdentity(businessNetworkConnection,owner.fn,
                        () =>{
                            return Utils.getVehicle(businessNetworkConnection,'V987654321')
                                .then(vehicle => {
                                    const newOwnerRelation =
                                        factory
                                        .newRelationship(
                                            namespace,
                                            Person,
                                            newOwner.$identifier);

                                    const changeOwner =
                                        factory
                                        .newTransaction(
                                            namespace,
                                            ChangeOwner);
                                    changeOwner.vin = vehicle.$identifier;
                                    changeOwner.registrationNumber =
                                        vehicle.registrationNumber;
                                    changeOwner.make = vehicle.make;

                                    const ownership =
                                        factory
                                        .newConcept(namespace, Ownership);
                                    const owners = new Array();
                                    owners.push(ownership);
                                    ownership.owner = ownerRelation;
                                    ownership.newOwner = newOwnerRelation;
                                    ownership.share = 1;
                                    ownership.state = WAITING;

                                    changeOwner.newOwners = owners;

                                    return businessNetworkConnection
                                        .submitTransaction(changeOwner)
                                        .then(() => {
                                            return Utils.getVehicle(businessNetworkConnection,
                                                vehicle.$identifier);
                                        }).then(result => {

                                            assert.equal(
                                                result.vin,
                                                vehicle.$identifier);
                                            assert.equal(
                                                result.make,
                                                vehicle.make);
                                            assert.equal(
                                                result.typeVersion,
                                                vehicle.typeVersion);
                                            assert.equal(
                                                result.registrationNumber,
                                                vehicle.registrationNumber);
                                            assert.equal(
                                              result
                                              .owners[0]
                                              .newOwner
                                              .$identifier,
                                              newOwner.$identifier);
                                            assert.equal(
                                              result.owners[0].state,
                                              WAITING);
                                        });
                                });
                    });
                });
        });
    it('should change the owner of a vehicle considering there will be ' +
        'more than one owner', () => {

            const factory =
                businessNetworkConnection
                .getBusinessNetwork()
                .getFactory();
            const newOwner =
                factory.newResource(namespace,Person,'333333222');
            newOwner.name = "João";
            newOwner.surname = "Cruz";
            newOwner.address = "Lisb";

            const newOwnerRelation =
                factory.newRelationship(namespace,Person,newOwner.$identifier);

            return Utils.addParticipant(businessNetworkConnection,Person,newOwner)
                .then(() => {
                     return Utils.usePersonIdentity(businessNetworkConnection,owner.fn,
                        () => {
                            return Utils.getVehicle(businessNetworkConnection,'V987654321')
                                .then( vehicle => {

                                    const ownerRelation =
                                        factory
                                        .newRelationship(
                                            namespace,
                                            Person,
                                            owner.$identifier);


                                    const changeOwner =
                                        factory
                                        .newTransaction(
                                            namespace,
                                            ChangeOwner);
                                    changeOwner.vin = vehicle.$identifier;
                                    changeOwner.registrationNumber =
                                        vehicle.registrationNumber;
                                    changeOwner.make = vehicle.make;

                                    const ownership =
                                        factory
                                        .newConcept(namespace,Ownership);
                                    ownership.owner = ownerRelation;
                                    ownership.newOwner = newOwnerRelation;
                                    ownership.share = .5;
                                    ownership.state = WAITING;

                                    const owners = new Array();
                                    owners.push(ownership);

                                    const ownership2 =
                                        factory
                                        .newConcept(namespace,Ownership);
                                    ownership2.owner = ownerRelation;
                                    ownership2.newOwner = ownerRelation;
                                    ownership2.share = .5;
                                    ownership2.state = WAITING;

                                    owners.push(ownership2);

                                    changeOwner.newOwners = owners;

                                    return businessNetworkConnection
                                        .submitTransaction(changeOwner)
                                        .then(() => {
                                            return Utils.getVehicle(businessNetworkConnection,
                                                vehicle.$identifier);

                                        }).then(result => {
                                            assert.equal(
                                                result.vin,
                                                vehicle.$identifier);
                                            assert.equal(
                                                result.make,
                                                vehicle.make);
                                            assert.equal(
                                                result.typeVersion,
                                                vehicle.typeVersion);
                                            assert.equal(
                                                result.registrationNumber,
                                                vehicle.registrationNumber);
                                            assert.equal(
                                              result
                                              .owners[0]
                                              .newOwner
                                              .$identifier,
                                              newOwner.$identifier);
                                            assert.equal(
                                              result
                                              .owners[1]
                                              .newOwner
                                              .$identifier,
                                              owner.$identifier);
                                            assert.equal(
                                                result.owners[0].share,
                                                0.5);
                                            assert.equal(
                                                result.owners[1].share,
                                                0.5);
                                            assert.equal(
                                                result.owners[0].state,
                                                WAITING);
                                            assert.equal(
                                                result.owners[1].state,
                                                WAITING);
                                        });
                                });
                        });
                });
        });

    it('should change one owner of a vehicle to another owner in a vehicle' +
        ' with multiple owners',
        () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const otherOwner =
                factory.newResource(namespace, Person,'123456789');
            otherOwner.surname = 'Cena';
            otherOwner.name = 'Paulo';
            otherOwner.address = 'London';
            return Utils.addParticipant(businessNetworkConnection,Person,otherOwner)
                .then(() => {
                    const otherOwnerRel =
                        factory
                        .newRelationship(
                            namespace,
                            Person,
                            otherOwner.$identifier);

                    const otherOwnership =
                        factory.newConcept(namespace, Ownership);

                    otherOwnership.owner = otherOwnerRel;
                    otherOwnership.share = 0.5;
                    otherOwnership.state = VALID;

                    const createVehicle =
                        factory.newTransaction(namespace, CreateVehicle);
                    const ownerRelation =
                        factory
                        .newRelationship(namespace, Person, owner.$identifier);
                    const ownership =
                        factory.newConcept(namespace, Ownership);

                    ownership.owner = ownerRelation;
                    ownership.share = 0.5;
                    ownership.state = VALID;

                    const owners = new Array();
                    owners.push(ownership);
                    owners.push(otherOwnership);

                    sampleVehicle.owners = owners;
                    sampleVehicle.certificateHolder = ownerRelation;

                    createVehicle.vehicle = sampleVehicle;

                    return businessNetworkConnection
                        .submitTransaction(createVehicle)
                        .then(() => {
                            const newOwner =
                                factory
                                .newResource(namespace,Person,'555666777');
                            newOwner.name = 'John';
                            newOwner.surname = 'Smith';
                            newOwner.address = 'Faro';

                            return Utils.addParticipant(businessNetworkConnection,Person,newOwner);
                        }).then(() => {
                            return
                                Utils.usePersonIdentity(businessNetworkConnection,
                                    owner.fn,
                                    () => {
                                        const newOwnerRelation =
                                            factory
                                            .newRelationship(
                                                namespace,
                                                Person,
                                                newOwner.$identifier);
                                        const changeOwner =
                                            factory
                                            .newTransaction(
                                                namespace,
                                                ChangeOwner);
                                        const ownership =
                                            factory
                                            .newConcept(
                                                namespace,
                                                Ownership);

                                        ownership.owner = owner;
                                        ownership.newOwner = newOwnerRelation;
                                        ownership.state = WAITING;
                                        ownership.share = 0.5;

                                        const owners = new Array();
                                        owners.push(ownership);

                                        changeOwner.vin = sampleVehicle.vin;
                                        changeOwner.registrationNumber =
                                            sampleVehicle.registrationNumber;
                                        changeOwner.make = sampleVehicle.make;
                                        changeOwner.owners = owners;

                                        return businessNetworkConnection
                                            .submitTransaction(changeOwner)
                                            .then(() => {
                                                return Utils.getVehicle(businessNetworkConnection,
                                                        sampleVehicle
                                                        .$identifier)
                                                    .then( result => {
                                                        assert.equal(
                                                            result.vin,
                                                            sampleVehicle.vin);
                                                        assert.equal(
                                                            result
                                                            .registrationNumber,
                                                            sampleVehicle
                                                            .registrationNumber);
                                                        console.log(result);
                                                        for(
                                                            var i = 0;
                                                            i < result.owners;
                                                            i++){
                                                            if(result
                                                                .owners[i]
                                                                .owner
                                                                .$identifier ==
                                                                owner
                                                                .$identifier &&
                                                               result
                                                               .owners[i]
                                                               .newOwner ==
                                                               newOwner
                                                               .$identifier &&
                                                               result
                                                               .owners[i]
                                                               .state ==
                                                               WAITING    &&
                                                               result
                                                               .owners[i]
                                                               .share == 0.5 ){
                                                                return assert
                                                                    .success();
                                                            }
                                                        }
                                                        return assert.fail();
                                                    });
                                    });
                            });
                            });
                });
        });
    it('should not change the owner if the transaction issuer is the owner'
            +' but is changing the ownership of other owner',
        () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const otherOwner =
                factory.newResource(namespace, Person,'123456789');
            otherOwner.surname = 'Cena';
            otherOwner.name = 'Paulo';
            otherOwner.address = 'London';
            return Utils.addParticipant(businessNetworkConnection,Person,otherOwner)
                .then(() => {
                    const otherOwnerRel =
                        factory
                        .newRelationship(
                            namespace,
                            Person,
                            otherOwner.$identifier);

                    const otherOwnership =
                        factory.newConcept(namespace, Ownership);

                    otherOwnership.owner = otherOwnerRel;
                    otherOwnership.share = 0.5;
                    otherOwnership.state = VALID;

                    const newOwner =
                        factory
                        .newResource(namespace,Person,'555666777');
                    newOwner.name = 'John';
                    newOwner.surname = 'Smith';
                    newOwner.address = 'Faro';

                    const createVehicle =
                        factory.newTransaction(namespace, CreateVehicle);
                    const ownerRelation =
                        factory
                        .newRelationship(namespace, Person, owner.$identifier);
                    const ownership =
                        factory.newConcept(namespace, Ownership);

                    ownership.owner = ownerRelation;
                    ownership.share = 0.5;
                    ownership.state = VALID;

                    const owners = new Array();
                    owners.push(ownership);
                    owners.push(otherOwnership);

                    sampleVehicle.owners = owners;
                    sampleVehicle.certificateHolder = ownerRelation;

                    createVehicle.vehicle = sampleVehicle;

                    return businessNetworkConnection
                        .submitTransaction(createVehicle)
                        .then(() => {
                            return Utils.addParticipant(businessNetworkConnection,Person,newOwner);
                        }).then(() => {
                            return Utils.usePersonIdentity(businessNetworkConnection,
                                    owner.fn,
                                    () => {
                                        const newOwnerRelation =
                                            factory
                                            .newRelationship(
                                                namespace,
                                                Person,
                                                newOwner.$identifier);
                                        const changeOwner =
                                            factory
                                            .newTransaction(
                                                namespace,
                                                ChangeOwner);
                                        const ownership =
                                            factory
                                            .newConcept(namespace,Ownership);

                                        ownership.owner = otherOwnerRel;
                                        ownership.newOwner = newOwnerRelation;
                                        ownership.state = WAITING;
                                        ownership.share = 0.5;

                                        const owners = new Array();
                                        owners.push(ownership);

                                        changeOwner.vin = sampleVehicle.vin;
                                        changeOwner.registrationNumber =
                                            sampleVehicle.registrationNumber;
                                        changeOwner.make = sampleVehicle.make;
                                        changeOwner.newOwners = owners;

                                        return businessNetworkConnection
                                            .submitTransaction(changeOwner)
                                            .should.eventually.be
                                            .rejectedWith(
                                                'Can not give more shares' +
                                                ' than the original owner has');
                            });
                        });
                });
        });
    it('should not change the owner if the share to be attributed is more that'+
        ' the owner has',
        () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const otherOwner =
                factory.newResource(namespace, Person,'123456789');
            otherOwner.surname = 'Cena';
            otherOwner.name = 'Paulo';
            otherOwner.address = 'London';
            return Utils.addParticipant(businessNetworkConnection,Person,otherOwner)
                .then(() => {
                    const otherOwnerRel =
                        factory
                        .newRelationship(
                            namespace,
                            Person,
                            otherOwner.$identifier);

                    const otherOwnership =
                        factory.newConcept(namespace, Ownership);

                    otherOwnership.owner = otherOwnerRel;
                    otherOwnership.share = 0.7;
                    otherOwnership.state = VALID;

                    const newOwner =
                        factory
                        .newResource(namespace,Person,'555666777');
                    newOwner.name = 'John';
                    newOwner.surname = 'Smith';
                    newOwner.address = 'Faro';

                    const createVehicle =
                        factory.newTransaction(namespace, CreateVehicle);
                    const ownerRelation =
                        factory
                        .newRelationship(namespace, Person, owner.$identifier);
                    const ownership =
                        factory.newConcept(namespace, Ownership);

                    ownership.owner = ownerRelation;
                    ownership.share = 0.3;
                    ownership.state = VALID;

                    const owners = new Array();
                    owners.push(ownership);
                    owners.push(otherOwnership);

                    sampleVehicle.owners = owners;
                    sampleVehicle.certificateHolder = ownerRelation;

                    createVehicle.vehicle = sampleVehicle;

                    return businessNetworkConnection
                        .submitTransaction(createVehicle)
                        .then(() => {
                            return Utils.addParticipant(businessNetworkConnection,Person,newOwner);
                        }).then(() => {
                            return Utils.usePersonIdentity(businessNetworkConnection,
                                    owner.fn,
                                    () => {
                                        const newOwnerRelation =
                                            factory
                                            .newRelationship(
                                                namespace,
                                                Person,
                                                newOwner.$identifier);
                                        const changeOwner =
                                            factory
                                            .newTransaction(
                                                namespace,
                                                ChangeOwner);
                                        const ownership =
                                            factory
                                            .newConcept(
                                                namespace,
                                                Ownership);

                                        ownership.owner = ownerRelation;
                                        ownership.newOwner = newOwnerRelation;
                                        ownership.state = WAITING;
                                        ownership.share = 0.5;

                                        const owners = new Array();
                                        owners.push(ownership);

                                        changeOwner.vin = sampleVehicle.vin;
                                        changeOwner.registrationNumber =
                                            sampleVehicle.registrationNumber;
                                        changeOwner.make = sampleVehicle.make;
                                        changeOwner.newOwners = owners;

                                        return businessNetworkConnection
                                            .submitTransaction(changeOwner)
                                            .should.eventually.be
                                            .rejectedWith(
                                                'Can not give more shares' +
                                                ' than the original owner has');
                            });
                        });
                });
        });

    it('should not change the owner if the share to be attributed is more that'+
        ' 1',
        () => {
            const factory =
              businessNetworkConnection.getBusinessNetwork().getFactory();

            const ownerRelation =
              factory.newRelationship(namespace,Person,owner.$identifier);

            const newOwner =
                factory.newResource(namespace,Person,'333333222');

            newOwner.name = "João";
            newOwner.surname = "Cruz";
            newOwner.address = "Lisb";

            return Utils.addParticipant(businessNetworkConnection,Person,newOwner)
                .then(() => {
                    return Utils.usePersonIdentity(businessNetworkConnection,owner.fn,
                        () => {
                            return Utils.getVehicle(businessNetworkConnection,'V987654321')
                                .then( vehicle => {
                                    const newOwnerRelation =
                                        factory
                                        .newRelationship(
                                            namespace,
                                            Person,
                                            newOwner
                                            .$identifier);

                                    const changeOwner =
                                        factory
                                        .newTransaction(
                                            namespace,
                                            ChangeOwner);
                                    changeOwner.vin = vehicle.$identifier;
                                    changeOwner.registrationNumber =
                                        vehicle.registrationNumber;
                                    changeOwner.make = vehicle.make;

                                    var ownership =
                                        factory
                                        .newConcept(namespace,Ownership);
                                    ownership.owner = ownerRelation;
                                    ownership.newOwner = newOwnerRelation;
                                    ownership.share = .7;
                                    ownership.state = WAITING;

                                    const owners = new Array();
                                    owners.push(ownership);

                                    ownership =
                                        factory
                                        .newConcept(namespace,Ownership);
                                    ownership.owner = ownerRelation;
                                    ownership.newOwner = ownerRelation;
                                    ownership.share = .5;
                                    ownership.state = WAITING;

                                    owners.push(ownership);

                                    changeOwner.newOwners = owners;
                                    return businessNetworkConnection
                                        .submitTransaction(changeOwner)
                                        .should.eventually.be
                                        .rejectedWith(
                                            'Can not give more shares than the'+
                                            ' original owner has');
                                });
                        });
                });
        });
    it('should not change the owner if the transaction issuer is not '
        +'the owner nor a judge',
        () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();
            //Create initial owner and new owner
            const newOwner =
                factory.newResource(namespace, Person, '333333222');
            const newOwnerRelation =
                factory
                .newRelationship(namespace, Person, newOwner.$identifier);

            return Utils.getVehicle(businessNetworkConnection,'V987654321')
                .then(vehicle => {
                    const changeOwner =
                        factory
                        .newTransaction(namespace, ChangeOwner);
                    changeOwner.vin = vehicle.$identifier;
                    changeOwner.registrationNumber =
                        vehicle.registrationNumber;
                    changeOwner.make = vehicle.make;
                    const ownership =
                        factory
                        .newConcept(
                            namespace,
                            Ownership);
                    ownership.owner = vehicle.owners[0].owner;
                    ownership.newOwner = newOwnerRelation;
                    ownership.share = 1;
                    ownership.state = WAITING;
                    const newOwners = new Array();
                    newOwners.push(ownership);
                    changeOwner.newOwners = newOwners;

                    return businessNetworkConnection
                      .submitTransaction(changeOwner)
                      .should.eventually.be
                      .rejectedWith(
                        'Only the owner is capable of changing ' +
                        'vehicle ownership.');
                });
          });
    it('should not change the owner if the make does not match the registry',
        () => {
            return Utils.usePersonIdentity(businessNetworkConnection,owner.fn,
                () => {
                    return Utils.getVehicle(businessNetworkConnection,'V987654321')
                        .then(vehicle => {
                            const factory =
                            businessNetworkConnection
                            .getBusinessNetwork().getFactory();

                            const newOwner =
                                factory
                                .newResource(
                                    namespace,
                                    Person,
                                    '333333222');
                            const newOwnerRelation =
                                factory
                                .newRelationship(
                                    namespace,
                                    Person,
                                    newOwner.$identifier);

                            const changeOwner =
                                factory
                                .newTransaction(namespace,ChangeOwner);
                            changeOwner.vin = vehicle.$identifier;
                            changeOwner.registrationNumber =
                                vehicle.registrationNumber;
                            changeOwner.make = 'Toyota';
                            const ownership =
                                factory.newConcept(namespace,Ownership);
                            ownership.owner = vehicle.owners[0].owner;
                            ownership.newOwner = newOwnerRelation;
                            ownership.share = 1;
                            ownership.state = WAITING;
                            const newOwners = new Array();
                            newOwners.push(ownership);

                            changeOwner.newOwners = newOwners;


                            return businessNetworkConnection
                                .submitTransaction(changeOwner)
                                .should.eventually.be
                                .rejectedWith(
                                    'Change owner data does not match'+
                                    ' the vehicle registry.');
                        });

            });

        });

    it('should change the vehicle ownership and become pending ownership' +
        ' until confirmation of a new owner',
        () => {

            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const newOwner = factory.newResource(namespace,Person,'333333222');
            newOwner.name = "João";
            newOwner.surname = "Cruz";
            newOwner.address = "Lisb";

            return Utils.addParticipant(businessNetworkConnection,Person,newOwner)
                .then(() => {
                    const newOwnerRelation =
                        factory
                        .newRelationship(namespace,Person,newOwner.$identifier);

                    return Utils.usePersonIdentity(businessNetworkConnection,owner.fn,
                        () => {
                            return Utils.getVehicle(businessNetworkConnection,'V987654321')
                                .then(vehicle => {

                                    const ownerRelation =
                                        factory
                                        .newRelationship(
                                            namespace,
                                            Person,
                                            owner.$identifier);

                                    const changeOwner =
                                        factory
                                        .newTransaction(
                                            namespace,
                                            ChangeOwner);

                                    changeOwner.vin = vehicle.$identifier;
                                    changeOwner.registrationNumber =
                                        vehicle.registrationNumber;
                                    changeOwner.make = vehicle.make;

                                    const ownership =
                                        factory
                                        .newConcept(namespace,Ownership);

                                    const owners = new Array();
                                    owners.push(ownership);
                                    ownership.owner = ownerRelation;
                                    ownership.newOwner = newOwnerRelation;
                                    ownership.share = 1;
                                    ownership.state = VALID;

                                    changeOwner.newOwners = owners;

                                    return businessNetworkConnection
                                        .submitTransaction(changeOwner)
                                        .then(() => {
                                            return Utils.getVehicle(businessNetworkConnection,
                                                vehicle.$identifier);
                                        }).then(result => {
                                            assert.equal(
                                                result.vin,
                                                vehicle.$identifier);
                                            assert.equal(
                                                result.make,
                                                vehicle.make);
                                            assert.equal(
                                                result.typeVersion,
                                                vehicle.typeVersion);
                                            assert.equal(
                                                result
                                                .registrationNumber,
                                                vehicle
                                                .registrationNumber);
                                            assert.equal(
                                                result
                                                .owners[0]
                                                .newOwner
                                                .$identifier,
                                                newOwner.$identifier);
                                            assert.equal(
                                                result.owners[0].state,
                                                WAITING);
                                        });
                                });
                        });

                });
        });

    it('should not change the owner if the vehicle is subject to a pending seizure associated with the same owner');
    it('should change the owner if the vehicle is subject to a pending seizure but the pending seizure is associated with another owner');
    });

});

