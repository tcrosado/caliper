const IdCard = require('composer-common').IdCard;
const AdminConnection = require('composer-admin').AdminConnection;
const CertificateUtil = require('composer-common').CertificateUtil;
const BusinessNetworkDefinition =
    require('composer-common').BusinessNetworkDefinition;
const path = require('path');

const namespace = 'pt.irn.bcar';
const Person = 'Person';
const assetType = 'Vehicle';

module.exports.cardStore = require('composer-common').NetworkCardStoreManager.getCardStore( { type: 'composer-wallet-inmemory' });
module.exports.adminConnection = new AdminConnection({cardStore: module.exports.cardStore});

before(async () => {

    // Embedded connection used for local testing
    const connectionProfile = {
      name: 'embedded',
      'x-type': 'embedded'
    };
    // Embedded connection does not need real credentials
    const credentials = CertificateUtil.generate({ commonName: 'admin' });

    // PeerAdmin identity used with the admin connection
    // to deploy business networks
    const deployerMetadata = {
      version: 1,
      userName: 'PeerAdmin',
      roles: ['PeerAdmin', 'ChannelAdmin']
    };
    const deployerCard = new IdCard(deployerMetadata, connectionProfile);
    deployerCard.setCredentials(credentials);

    const deployerCardName = 'PeerAdmin';

    return module.exports.adminConnection
        .importCard(deployerCardName, deployerCard)
        .then(() => {
            return module.exports.adminConnection.connect(deployerCardName);
        })

});

module.exports.setup = function(businessNetworkConnection){
  const adminUserName = 'admin';
  let adminCardName;
  let businessNetworkDefinition;
  return BusinessNetworkDefinition
        .fromDirectory(path.resolve(__dirname, '..'))
        .then(definition => {
            businessNetworkDefinition = definition;
            // Install the Composer runtime for the new business network
            return module.exports.adminConnection.install(businessNetworkDefinition);

          }).then(() => {
          // Start the business network and configure an network admin identity
          const startOptions = {
              networkAdmins: [
                  {
                      userName: adminUserName,
                      enrollmentSecret: 'adminpw'
                    }
              ]
            };
          return module.exports.adminConnection.start(businessNetworkDefinition.getName(),businessNetworkDefinition.getVersion() ,startOptions);

        }).then(adminCards => {
          // Import the network admin identity for us to use
          adminCardName =
              `${adminUserName}@${businessNetworkDefinition.getName()}`;
          return module.exports.adminConnection
              .importCard(adminCardName, adminCards.get(adminUserName));

        }).then(() => {
          // Connect to the business network using the network admin identity
          return businessNetworkConnection.connect(adminCardName);
        })
}

module.exports.useIdentity = function (businessNetworkConnection,type,userId,func){
    return businessNetworkConnection
        .issueIdentity(namespace+'.'+type+"#"+userId,userId)
        .then((result) => {

            const connectionProfile = {
              name: 'embedded',
              'x-type': 'embedded'
            };
            const metadata = {
              version: 1,
              userName: userId,
              enrollmentSecret: result.userSecret,
              businessNetwork: 'bcar-network'
            };

            const card = new IdCard(metadata,connectionProfile);
            const cardName = userId+'@bcar-network';
            return module.exports.adminConnection
                .importCard(cardName,card)
                .then(() => {
                    return businessNetworkConnection
                        .connect(cardName)
                        .then(() => {
                            return func();
                        });
                });
        });
    }

module.exports.usePersonIdentity = function (businessNetworkConnection,userId,func){
        return module.exports.useIdentity(businessNetworkConnection,Person,userId,func);
    }

module.exports.getVehicle =  function (businessNetworkConnection,vin){
        return businessNetworkConnection
          .getAssetRegistry(namespace + '.' + assetType)
          .then(registry => {
              return registry.get(vin)
            });
    }

module.exports.addParticipant = function (businessNetworkConnection,type,participant) {
        return businessNetworkConnection
            .getParticipantRegistry(namespace + '.' + type)
            .then(registry => {
                return registry.add(participant);
            });
    }