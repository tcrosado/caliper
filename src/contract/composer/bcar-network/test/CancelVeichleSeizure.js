'use strict';
/**
 * Write the unit tests for your transction processor functions here
 */

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection =
    require('composer-client').BusinessNetworkConnection;
const BusinessNetworkDefinition =
    require('composer-common').BusinessNetworkDefinition;
const IdCard = require('composer-common').IdCard;
const MemoryCardStore = require('composer-common').MemoryCardStore;

const path = require('path');
const chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var assert = require('assert');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const namespace = 'pt.irn.bcar';
const assetType = 'Vehicle';
const Person = 'Person';
const Company = 'Company';
const RegisterAsGarantee = 'RegisterAsGarantee';
const JudicialOfficer = 'JudicialOfficer';
const IssuePendingSeizure = 'IssuePendingSeizure';
const IssueSeizure = 'IssueSeizure';
const CancelSeizure = 'CancelSeizure';
const Ownership = 'Ownership';
const VALID = 'VALID';
const WAITING = 'WAITING';
const STATE_ACTIVE = 'ACTIVE';

// Required to execute (before) setup function when the test starts
var Utils = require('./before.js');

describe('#'+namespace,() => {

    let businessNetworkConnection;
    let judicialOfficer;
    let owner;
    let companyOwner;
    let creditor;
    let creditorOwner;
    let creditorRelation;
    let vehicle;
    let pendingSeizure;
    let cancelSeizure;
    let seizure;

    beforeEach(() => {
      businessNetworkConnection =
        new BusinessNetworkConnection({cardStore: Utils.cardStore});

      return Utils.setup(businessNetworkConnection)
        .then(async () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();
            // Setting up a judicial Officer
            judicialOfficer =
                factory.newResource(namespace, JudicialOfficer, '234432234');
            judicialOfficer.name = "José";
            judicialOfficer.surname = "Matos";
            judicialOfficer.address = "Rua S. Nunca - Porto";
            judicialOfficer.court = "Tribunal Arbitral de Lisboa";


            // Setting up a creditor
            creditor = factory.newResource(namespace, Company, '111111111');
            creditor.name = "United Bank";
            creditor.address = "Republica 10";

            //Setting up a creditor company owner
            creditorOwner =
                factory.newResource(namespace, Person,'123456789');
            creditorOwner.name = 'Manuel';
            creditorOwner.surname = 'Gomes';
            creditorOwner.address = 'Lisbon';

            const creditorOwnerRel =
                factory
                .newRelationship(namespace, Person, creditorOwner.$identifier);
            var creditorOwners = new Array();
            creditorOwners.push(creditorOwnerRel);
            creditor.owners = creditorOwners;


            // Setting up a company
            owner = factory.newResource(namespace, Company, '222333222');
            owner.name = 'Leasing e Carros LDA';
            owner.address = 'Liberdade 21';

            //Setting up company owner
            companyOwner =
                factory
                .newResource(namespace, Person, '222555888');

            companyOwner.name = 'Richard';
            companyOwner.surname = 'Hammond'
            companyOwner.address = 'Paris';
            const companyOwnerRelation =
                factory
                .newRelationship(namespace, Person, companyOwner.$identifier);
            var owners = new Array();
            owners.push(companyOwnerRelation);
            owner.owners = owners;

            // Setting up a car ownership using a company
            const ownerRelation =
                factory.newRelationship(namespace, Company, owner.$identifier);
            const ownership = factory.newConcept(namespace, Ownership);
            owners = new Array();
            owners.push(ownership);
            ownership.owner = ownerRelation;
            ownership.share = 1;
            ownership.state = VALID;

            //Create vehicle and insert it in the blockchain
            vehicle =
                factory.newResource(namespace, assetType, 'V987654321');

            vehicle.registrationNumber = '00-AA-00';
            vehicle.make = 'Porsche';
            vehicle.typeVersion = '911';
            vehicle.owners = owners;
            vehicle.certificateHolder = ownerRelation;
            vehicle.category = 'M';
            vehicle.vClass = 'LIGHT';
            vehicle.maxLadenMass = 3500;
            const engine = factory.newConcept(namespace, 'Engine');
            engine.capacity = 0;
            engine.fuelType = 'GASOLINE';
            engine.ratedSpeed = 0;
            engine.ein = 'E10';
            vehicle.engine = engine;
            vehicle.state = STATE_ACTIVE;

            const emissions = factory.newConcept(namespace, 'Emissions');
            emissions.emCO = 10;
            emissions.emHC = 10;
            emissions.emNOx = 10;
            emissions.emHCNOx = 10;
            emissions.emCO2 = 10;
            emissions.emFuelConsumption = 7.5;

            vehicle.emissions = emissions;

            var creditorRelation =
                factory.newRelationship(namespace,Company,creditor.$identifier);

            cancelSeizure =
                factory.newTransaction(namespace,CancelSeizure);
            cancelSeizure.owner = ownerRelation;
            cancelSeizure.creditor = creditorRelation;
            cancelSeizure.totalValue = 150000;
            cancelSeizure.vin = vehicle.vin;
            cancelSeizure.registrationNumber =
                vehicle.registrationNumber;
            cancelSeizure.make = vehicle.make;

            pendingSeizure =
                factory.newTransaction(namespace,IssuePendingSeizure);

            pendingSeizure.owner = ownerRelation;
            pendingSeizure.creditor = creditorRelation;
            pendingSeizure.totalValue = 150000;
            pendingSeizure.vin = vehicle.vin;
            pendingSeizure.registrationNumber = vehicle.registrationNumber;
            pendingSeizure.make = vehicle.make;

            seizure =
                factory.newTransaction(namespace,IssueSeizure);

            seizure.owner = ownerRelation;
            seizure.creditor = creditorRelation;
            seizure.totalValue = 150000;
            seizure.vin = vehicle.vin;
            seizure.registrationNumber = vehicle.registrationNumber;
            seizure.make = vehicle.make;;
            seizure.date = new Date();
            seizure.orderNumber = 'G10/2018';

            //Adding owner to registry
            var promiseCreditor = Utils.addParticipant(
                    businessNetworkConnection,
                    Company,
                    creditor);
            var promiseCredOwner = Utils.addParticipant(
                    businessNetworkConnection,
                    Person,
                    creditorOwner);
            var promiseOwner = Utils.addParticipant(
                    businessNetworkConnection,
                    Company,
                    owner);
            var promiseCompanyOwner = Utils.addParticipant(
                    businessNetworkConnection,
                    Person,
                    companyOwner);
            var promiseJudicialOfficer = Utils.addParticipant(
                    businessNetworkConnection,
                    JudicialOfficer,
                    judicialOfficer);

            var promiseVehicle = businessNetworkConnection
                .getAssetRegistry(namespace + '.' + assetType)
                .then(registry => {
                    return registry.add(vehicle);
                });


            await Promise.all(
                [promiseCreditor,promiseCredOwner,promiseOwner,
                promiseCompanyOwner,promiseVehicle,promiseJudicialOfficer]);
          });
    });

    describe('CancelSeizure()', () =>{
        it('should cancel a pending seizure', () =>{

            return Utils.useIdentity(
                businessNetworkConnection,
                JudicialOfficer,
                judicialOfficer.$identifier,
                () =>{
                    return businessNetworkConnection
                        .submitTransaction(pendingSeizure)
                        .then( () => {
                            return businessNetworkConnection
                                .submitTransaction(cancelSeizure);
                        })
                        .then(() => {
                            return Utils
                                .getVehicle(
                                    businessNetworkConnection,
                                    vehicle.$identifier)
                                .then( (result) => {
                                   assert.equal(result.seizure,undefined);
                                });
                        });
                });
        });

        it('should not cancel a pending seizure if it is not done by a' +
            ' judicial officer', () =>{

              return Utils.useIdentity(
                businessNetworkConnection,
                JudicialOfficer,
                judicialOfficer.$identifier,
                () =>{
                    return businessNetworkConnection
                        .submitTransaction(pendingSeizure)
                        .then( () => {
                            return businessNetworkConnection
                                .connect('admin@bcar-network');
                        })
                        .then(()=>{
                            return Utils.usePersonIdentity(
                                businessNetworkConnection,
                                creditorOwner.$identifier,
                                () => {
                                    return businessNetworkConnection
                                        .submitTransaction(
                                            cancelSeizure)
                                        .should.eventually.be
                                        .rejectedWith(
                                            'does not have \'CREATE\'' +
                                            ' access to resource');
                                });
                        });
                });
        });
    })
});