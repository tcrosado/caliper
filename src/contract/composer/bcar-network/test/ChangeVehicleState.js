'use strict';

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection =
    require('composer-client').BusinessNetworkConnection;
const BusinessNetworkDefinition =
    require('composer-common').BusinessNetworkDefinition;
const IdCard = require('composer-common').IdCard;
const MemoryCardStore = require('composer-common').MemoryCardStore;

const path = require('path');
const chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var assert = require('assert');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const namespace = 'pt.irn.bcar';
const assetType = 'Vehicle';
const Person = 'Person';
const Company = 'Company';
const Ownership = 'Ownership';
const ChangeState = 'ChangeState';
const RegistryEmployee = 'RegistryEmployee';
const STATE_ACTIVE = 'ACTIVE';
const STATE_SUSPENDED = 'SUSPENDED';
const STATE_INACTIVE = 'INACTIVE';
const STATE_DESTRUCTED = 'DESTRUCTED';
const STATE_STOLEN = 'STOLEN';
const VALID = 'VALID';

// Required to execute (before) setup function when the test starts
var Utils = require('./before.js');

describe('#'+namespace,() => {

    let businessNetworkConnection;
    let employee;
    let owner;
    let companyOwner;
    let creditor;
    let creditorOwner;
    let vehicle;
    let seizure;

    beforeEach(() => {
      businessNetworkConnection =
        new BusinessNetworkConnection({cardStore: Utils.cardStore});

      return Utils.setup(businessNetworkConnection)
        .then(async () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            //Setup registry employee

            employee = factory.newResource(namespace, RegistryEmployee, '111888777');
            employee.name =  'João'
            employee.surname = 'Guedes'
            employee.address = 'Lisboa';
            employee.employeeNr = 'LIS20-P19';

            // Setting up a company
            owner = factory.newResource(namespace, Company, '222333222');
            owner.name = 'Leasing e Carros LDA';
            owner.address = 'Liberdade 21';

            //Setting up company owner
            companyOwner =
                factory
                .newResource(namespace, Person, '222555888');

            companyOwner.name = 'Richard';
            companyOwner.surname = 'Hammond'
            companyOwner.address = 'Paris';
            const companyOwnerRelation =
                factory
                .newRelationship(namespace, Person, companyOwner.$identifier);
            var owners = new Array();
            owners.push(companyOwnerRelation);
            owner.owners = owners;

            // Setting up a car ownership using a company
            const ownerRelation =
                factory.newRelationship(namespace, Company, owner.$identifier);
            const ownership = factory.newConcept(namespace, Ownership);
            owners = new Array();
            owners.push(ownership);
            ownership.owner = ownerRelation;
            ownership.share = 0.5;
            ownership.state = VALID;

            const ownershipPerson = factory.newConcept(namespace,Ownership);
            ownershipPerson.owner = companyOwnerRelation;
            ownershipPerson.share = 0.5;
            ownershipPerson.state = VALID;

            //Create vehicle and insert it in the blockchain
            vehicle =
                factory.newResource(namespace, assetType, 'V987654321');

            vehicle.registrationNumber = '00-AA-00';
            vehicle.make = 'Porsche';
            vehicle.typeVersion = '911';
            vehicle.owners = owners;
            vehicle.certificateHolder = ownerRelation;
            vehicle.category = 'M';
            vehicle.vClass = 'LIGHT';
            vehicle.maxLadenMass = 3500;
            const engine = factory.newConcept(namespace, 'Engine');
            engine.capacity = 0;
            engine.fuelType = 'GASOLINE';
            engine.ratedSpeed = 0;
            engine.ein = 'E10';
            vehicle.engine = engine;

            const emissions = factory.newConcept(namespace, 'Emissions');
            emissions.emCO = 10;
            emissions.emHC = 10;
            emissions.emNOx = 10;
            emissions.emHCNOx = 10;
            emissions.emCO2 = 10;
            emissions.emFuelConsumption = 7.5;

            vehicle.emissions = emissions;
            vehicle.state = STATE_ACTIVE;



            //Adding owner to registry
            var promiseOwner = Utils.addParticipant(
                    businessNetworkConnection,
                    Company,
                    owner);

            var promiseCompanyOwner = Utils.addParticipant(
                    businessNetworkConnection,
                    Person,
                    companyOwner);

            var promiseEmployee = Utils.addParticipant(
                    businessNetworkConnection,
                    RegistryEmployee,
                    employee);

            var promiseVehicle = businessNetworkConnection
                .getAssetRegistry(namespace + '.' + assetType)
                .then(registry => {
                    return registry.add(vehicle);
                });


            await Promise.all(
                [promiseOwner,promiseCompanyOwner,
                promiseVehicle,promiseEmployee]);
          });
    });
    describe('ChangeState()', () => {
        it('should ensure that vehicle initial state is ACTIVE', () => {
            const factory =
                businessNetworkConnection
                .getBusinessNetwork()
                .getFactory();

            return Utils.getVehicle(
                    businessNetworkConnection,
                    vehicle.$identifier)
                .then( (result) => {
                    assert.equal(result.state, STATE_ACTIVE, 'Vehicle\'s initial state should be ACTIVE');
                })
        });

        it('should change vehicle state to STOLEN', () => {
            const factory =
                businessNetworkConnection
                .getBusinessNetwork()
                .getFactory();

            return Utils.useIdentity(businessNetworkConnection,RegistryEmployee,employee.$identifier, () => {
                const changeState =
                    factory
                    .newTransaction(namespace,
                        ChangeState);
                changeState.vin = vehicle.$identifier;
                changeState.registrationNumber = vehicle.registrationNumber;
                changeState.make = vehicle.make;
                changeState.newState = STATE_STOLEN;

                return businessNetworkConnection.submitTransaction(changeState)
                .then(() => {
                    return  Utils.getVehicle(businessNetworkConnection,vehicle.$identifier)
                        .then( result => {
                            assert.equal(result.state, changeState.newState,'Vehicle\'s state was not updated')
                        });
                })
            });
        });
        it('should change vehicle state to DESTRUCTED', () => {
            const factory =
                businessNetworkConnection
                .getBusinessNetwork()
                .getFactory();

            return Utils.useIdentity(businessNetworkConnection,RegistryEmployee,employee.$identifier, () => {
                const changeState =
                    factory
                    .newTransaction(namespace,
                        ChangeState);
                changeState.vin = vehicle.$identifier;
                changeState.registrationNumber = vehicle.registrationNumber;
                changeState.make = vehicle.make;
                changeState.newState = STATE_DESTRUCTED;

                return businessNetworkConnection.submitTransaction(changeState)
                .then(() => {
                    return  Utils.getVehicle(businessNetworkConnection,vehicle.$identifier)
                        .then( result => {
                            assert.equal(result.state, changeState.newState,'Vehicle\'s state was not updated')
                        });
                })
            });
        });
        it('should change vehicle state to INACTIVE', () => {
            const factory =
                businessNetworkConnection
                .getBusinessNetwork()
                .getFactory();

            return Utils.useIdentity(businessNetworkConnection,RegistryEmployee,employee.$identifier, () => {
                const changeState =
                    factory
                    .newTransaction(namespace,
                        ChangeState);
                changeState.vin = vehicle.$identifier;
                changeState.registrationNumber = vehicle.registrationNumber;
                changeState.make = vehicle.make;
                changeState.newState = STATE_INACTIVE;

                return businessNetworkConnection.submitTransaction(changeState)
                .then(() => {
                    return  Utils.getVehicle(businessNetworkConnection,vehicle.$identifier)
                        .then( result => {
                            assert.equal(result.state, changeState.newState,'Vehicle\'s state was not updated')
                        });
                })
            });
        });
        it('should change vehicle state to SUSPENDED', () => {
            const factory =
                businessNetworkConnection
                .getBusinessNetwork()
                .getFactory();

            return Utils.useIdentity(businessNetworkConnection,RegistryEmployee,employee.$identifier, () => {
                const changeState =
                    factory
                    .newTransaction(namespace,
                        ChangeState);
                changeState.vin = vehicle.$identifier;
                changeState.registrationNumber = vehicle.registrationNumber;
                changeState.make = vehicle.make;
                changeState.newState = STATE_SUSPENDED;

                return businessNetworkConnection.submitTransaction(changeState)
                .then(() => {
                    return  Utils.getVehicle(businessNetworkConnection,vehicle.$identifier)
                        .then( result => {
                            assert.equal(result.state, changeState.newState,'Vehicle\'s state was not updated')
                        });
                })
            });
        });
        it('should ensure only a registry employee can execute this action', () => {
            const factory =
                businessNetworkConnection
                .getBusinessNetwork()
                .getFactory();

            return Utils.usePersonIdentity(businessNetworkConnection,companyOwner.$identifier, () => {
                const changeState =
                    factory
                    .newTransaction(namespace,
                        ChangeState);
                changeState.vin = vehicle.$identifier;
                changeState.registrationNumber = vehicle.registrationNumber;
                changeState.make = vehicle.make;
                changeState.newState = STATE_SUSPENDED;

                return businessNetworkConnection.submitTransaction(changeState)
                    .should.eventually.be
                    .rejectedWith(
                        'does not have \'CREATE\' access to resource');
            });
        });

    });
});