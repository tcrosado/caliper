'use strict';
/**
 * Write the unit tests for your transction processor functions here
 */

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection =
    require('composer-client').BusinessNetworkConnection;
const BusinessNetworkDefinition =
    require('composer-common').BusinessNetworkDefinition;
const IdCard = require('composer-common').IdCard;
const MemoryCardStore = require('composer-common').MemoryCardStore;

const path = require('path');
const chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var assert = require('assert');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const namespace = 'pt.irn.bcar';
const assetType = 'Vehicle';
const Person = 'Person';
const Company = 'Company';
const CreateLease = 'CreateLease';
const Ownership = 'Ownership';
const VALID = 'VALID';
const WAITING = 'WAITING';
const STATE_ACTIVE = 'ACTIVE';

// Required to execute (before) setup function when the test starts
var Utils = require('./before.js');

describe('#'+namespace,() => {

	let businessNetworkConnection;
 	let owner;

  	beforeEach(() => {
      businessNetworkConnection =
        new BusinessNetworkConnection({cardStore: Utils.cardStore});

      return Utils.setup(businessNetworkConnection)
        .then(() => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            owner = factory.newResource(namespace, Company, '222333222');
            owner.name = 'Leasing e Carros LDA';
            owner.address = 'Liberdade 21';



            //Setting up company owner
            const companyOwner =
            	factory
            	.newResource(namespace, Person, '2225588');

            companyOwner.name = 'Richard';
            companyOwner.surname = 'Hammond'
            companyOwner.address = 'Paris';
            const companyOwnerRelation =
            	factory
            	.newRelationship(namespace, Person, companyOwner.$identifier);
            var owners = new Array();
            owners.push(companyOwnerRelation);
            owner.owners = owners;

            const ownerRelation =
                factory.newRelationship(namespace, Company, owner.$identifier);
            const ownership = factory.newConcept(namespace, Ownership);
            owners = new Array();
            owners.push(ownership);
            ownership.owner = ownerRelation;
            ownership.share = 1;
            ownership.state = VALID;

            //Create vehicle and insert it in the blockchain
            const vehicle =
                factory.newResource(namespace, assetType, 'V987654321');

            vehicle.registrationNumber = '00-AA-00';
            vehicle.make = 'Porsche';
            vehicle.typeVersion = '911';
            vehicle.owners = owners;
            vehicle.certificateHolder = ownerRelation;
            vehicle.category = 'M';
            vehicle.vClass = 'LIGHT';
            vehicle.maxLadenMass = 3500;
            const engine = factory.newConcept(namespace, 'Engine');
            engine.capacity = 0;
            engine.fuelType = 'GASOLINE';
            engine.ratedSpeed = 0;
            engine.ein = 'E10';
            vehicle.engine = engine;
            vehicle.state = STATE_ACTIVE;

            const emissions = factory.newConcept(namespace, 'Emissions');
            emissions.emCO = 10;
            emissions.emHC = 10;
            emissions.emNOx = 10;
            emissions.emHCNOx = 10;
            emissions.emCO2 = 10;
            emissions.emFuelConsumption = 7.5;

            vehicle.emissions = emissions;

            //Adding owner to registry
            var promiseAddCompany = Utils.addParticipant(businessNetworkConnection,Company,owner);
            var promiseAddVehicle = businessNetworkConnection
                .getAssetRegistry(namespace + '.' + assetType)
                .then(registry => {
                    return registry.add(vehicle);
                  });
            return Promise.all([promiseAddCompany,promiseAddVehicle]);
          });
    });


	describe('Lease()',() => {
		it('should register a leasing contract Company to Person',
			() => {
				const factory =
					businessNetworkConnection.getBusinessNetwork().getFactory();

				const carUser =
					factory.newResource(namespace,Person,'123456789');
				carUser.name = 'João';
				carUser.surname = 'Santos';
				carUser.address = 'Praceta Camões';

				const createLease = factory.newTransaction(namespace,CreateLease);
				createLease.startDate = new Date();
				createLease.endDate =
					new Date(
						createLease.startDate.getFullYear()+5,
						createLease.startDate.getMonth(),
						createLease.startDate.getDay(),
						createLease.startDate.getHours(),
						createLease.startDate.getMinutes(),
						createLease.startDate.getSeconds(),
						createLease.startDate.getMilliseconds());
				createLease.vin = 'V987654321';
				createLease.make = 'Porsche';
				createLease.registrationNumber = '00-AA-00';
				createLease.totalValue= 75000;
				createLease.lessee =
					factory
					.newRelationship(namespace,Person,carUser.$identifier);
				return Utils
					.useIdentity(
						businessNetworkConnection,
						Company,
						owner.$identifier,
						() => {
							return businessNetworkConnection
								.submitTransaction(createLease)
								.then(() => {
									return Utils
										.getVehicle(
											businessNetworkConnection,
											createLease.vin)
										.then( result => {
											assert.notEqual(
												result.lease,
												undefined);
											assert.equal(
												result.lease.startDate.getTime(),
												createLease.startDate.getTime());
											assert.equal(
												result.lease.endDate.getTime(),
												createLease.endDate.getTime());
											assert.equal(
												result.lease.totalValue,
												createLease.totalValue);
											assert.equal(
												result.vin,
												createLease.vin);
											assert.equal(
												result.lease.lessee.$identifier,
												createLease.lessee.$identifier);
                                            assert.equal(
                                                result.lease.state,
                                                WAITING);
										});
								});
					});
		});

		it('should register a leasing contract Company to Company',
			() => {
				const factory =
					businessNetworkConnection.getBusinessNetwork().getFactory();

				const carUser =
					factory.newResource(namespace,Person,'123456789');
				carUser.name = 'João';
				carUser.surname = 'Santos';
				carUser.address = 'Praceta Camões';

				const carUserCompany =
					factory.newResource(namespace,Company,'999888777');
				carUserCompany.name = 'OutsourceIT';
				carUserCompany.address = 'Lisbon';
				const owners = new Array();
				owners.push(carUser);
				carUserCompany.owners = owners;

				return Utils
					.useIdentity(
						businessNetworkConnection,
						Company,
						owner.$identifier,
						()=>{

							const createLease =
								factory.newTransaction(namespace,CreateLease);
							createLease.startDate = new Date();
							createLease.endDate =
								new Date(
									createLease.startDate.getFullYear()+5,
									createLease.startDate.getMonth(),
									createLease.startDate.getDay(),
									createLease.startDate.getHours(),
									createLease.startDate.getMinutes(),
									createLease.startDate.getSeconds(),
									createLease.startDate.getMilliseconds());
							createLease.vin = 'V987654321';
							createLease.make = 'Porsche';
							createLease.registrationNumber = '00-AA-00';
							createLease.totalValue= 75000;
							createLease.lessee =
								factory
								.newRelationship(
									namespace,
									Company,
									carUserCompany.$identifier);

							return businessNetworkConnection
								.submitTransaction(createLease)
								.then(() => {
									return Utils
										.getVehicle(
											businessNetworkConnection,
											createLease.vin)
										.then( result => {
											assert.notEqual(
												result.lease,
												undefined);
											assert.equal(
												result.lease.startDate.getTime(),
												createLease.startDate.getTime());
											assert.equal(
												result.lease.endDate.getTime(),
												createLease.endDate.getTime());
											assert.equal(
												result.lease.totalValue,
												createLease.totalValue);
											assert.equal(
												result.vin,
												createLease.vin);
											assert.equal(
												result.lease.lessee.$identifier,
												createLease.lessee.$identifier);
                                            assert.equal(
                                                result.lease.state,
                                                WAITING);
										});
								});
					});

		});

		it('should not register a leasing contract with endDate on a past day',
			() => {
				const factory =
					businessNetworkConnection.getBusinessNetwork().getFactory();

				const carUser =
					factory.newResource(namespace,Person,'123456789');
				carUser.name = 'João';
				carUser.surname = 'Santos';
				carUser.address = 'Praceta Camões';

				const createLease = factory.newTransaction(namespace,CreateLease);
				createLease.startDate = new Date(2000,1,1,1,0,0,0);
				createLease.endDate =
					new Date(
						createLease.startDate.getFullYear()+1,
						createLease.startDate.getMonth(),
						createLease.startDate.getDay(),
						createLease.startDate.getHours(),
						createLease.startDate.getMinutes(),
						createLease.startDate.getSeconds(),
						createLease.startDate.getMilliseconds());
				createLease.vin = 'V987654321';
				createLease.make = 'Porsche';
				createLease.registrationNumber = '00-AA-00';
				createLease.totalValue= 75000;
				createLease.lessee =
					factory
					.newRelationship(namespace,Person,carUser.$identifier);
				return Utils
					.useIdentity(
						businessNetworkConnection,
						Company,
						owner.$identifier,
						() => {
							return businessNetworkConnection
								.submitTransaction(createLease)
								.should.eventually.be
								.rejectedWith(
									'End date must be greater than current date.');
					});
		});

		it('should not register a leasing contract with end Date earlier' +
			' than start Date',
			() => {
				const factory =
					businessNetworkConnection.getBusinessNetwork().getFactory();

				const carUser =
					factory.newResource(namespace,Person,'123456789');
				carUser.name = 'João';
				carUser.surname = 'Santos';
				carUser.address = 'Praceta Camões';

				const createLease = factory.newTransaction(namespace,CreateLease);
				createLease.startDate = new Date(2019,1,1,1,0,0,0);
				createLease.endDate =
					new Date(
						createLease.startDate.getFullYear()-1,
						createLease.startDate.getMonth(),
						createLease.startDate.getDay(),
						createLease.startDate.getHours(),
						createLease.startDate.getMinutes(),
						createLease.startDate.getSeconds(),
						createLease.startDate.getMilliseconds());
				createLease.vin = 'V987654321';
				createLease.make = 'Porsche';
				createLease.registrationNumber = '00-AA-00';
				createLease.totalValue= 75000;
				createLease.lessee =
					factory
					.newRelationship(namespace,Person,carUser.$identifier);
				return Utils
					.useIdentity(
						businessNetworkConnection,
						Company,
						owner.$identifier,
						() => {
							return businessNetworkConnection
								.submitTransaction(createLease)
								.should.eventually.be
								.rejectedWith(
									'End date must be greater than'+
									' start date.');
				});
		});

		it('should not register a lease contract if the issuer' +
			' is not the owner',
			() => {
				const factory =
					businessNetworkConnection.getBusinessNetwork().getFactory();

				const carUser =
					factory.newResource(namespace,Person,'123456789');
				carUser.name = 'João';
				carUser.surname = 'Santos';
				carUser.address = 'Praceta Camões';

				const createLease = factory.newTransaction(namespace,CreateLease);
				createLease.startDate = new Date();
				createLease.endDate =
					new Date(
						createLease.startDate.getFullYear()+5,
						createLease.startDate.getMonth(),
						createLease.startDate.getDay(),
						createLease.startDate.getHours(),
						createLease.startDate.getMinutes(),
						createLease.startDate.getSeconds(),
						createLease.startDate.getMilliseconds());
				createLease.vin = 'V987654321';
				createLease.make = 'Porsche';
				createLease.registrationNumber = '00-AA-00';
				createLease.totalValue= 75000;
				createLease.lessee =
					factory
					.newRelationship(namespace,Person,carUser.$identifier);
				return Utils
					.addParticipant(businessNetworkConnection,Person,carUser)
					.then(
						() => {
							return Utils
								.useIdentity(
									businessNetworkConnection,
									Person,
									carUser.$identifier,
								() => {
									return businessNetworkConnection
										.submitTransaction(createLease)
										.should.eventually.be
										.rejectedWith(
											'Only the owner can create a'+
											' lease contract');
								});
					});

			});
	});
});