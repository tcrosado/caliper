'use strict';
/**
 * Write the unit tests for your transction processor functions here
 */

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection =
    require('composer-client').BusinessNetworkConnection;
const BusinessNetworkDefinition =
    require('composer-common').BusinessNetworkDefinition;
const IdCard = require('composer-common').IdCard;
const MemoryCardStore = require('composer-common').MemoryCardStore;

const path = require('path');
const chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var assert = require('assert');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const namespace = 'pt.irn.bcar';
const assetType = 'Vehicle';
const Person = 'Person';
const Judge = 'Judge';
const Inspector = 'Inspector';
const VALID = 'VALID';
const WAITING = 'WAITING';
const Ownership = 'Ownership';
const ConfirmOwnership = 'ConfirmOwnership';
const CreateVehicle = 'CreateVehicle';
const ChangeOwner = 'ChangeOwner';
const STATE_ACTIVE = 'ACTIVE';


// Required to execute (before) setup function when the test starts
var Utils = require('./before.js');

describe('#' + namespace, () => {
  let businessNetworkConnection;
  let owner;

  beforeEach(() => {
      businessNetworkConnection =
        new BusinessNetworkConnection({cardStore: Utils.cardStore});

      return Utils.setup(businessNetworkConnection)
        .then(() => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            owner = factory.newResource(namespace, Person, '222333222');
            owner.surname = 'Santos';
            owner.name = 'José';
            owner.address = 'Liberdade 21';

            const ownerRelation =
                factory.newRelationship(namespace, Person, owner.$identifier);

            const ownership = factory.newConcept(namespace, Ownership);
            const owners = new Array();
            owners.push(ownership);
            ownership.owner = ownerRelation;
            ownership.share = 1;
            ownership.state = VALID;

            //Create vehicle and insert it in the blockchain
            const vehicle =
                factory.newResource(namespace, assetType, 'V987654321');

            vehicle.registrationNumber = '00-AA-00';
            vehicle.make = 'Porsche';
            vehicle.typeVersion = '911';
            vehicle.owners = owners;
            vehicle.certificateHolder = ownerRelation;
            vehicle.category = 'M';
            vehicle.vClass = 'LIGHT';
            vehicle.maxLadenMass = 3500;
            const engine = factory.newConcept(namespace, 'Engine');
            engine.capacity = 0;
            engine.fuelType = 'GASOLINE';
            engine.ratedSpeed = 0;
            engine.ein = 'E10';
            vehicle.engine = engine;
            vehicle.state = STATE_ACTIVE;

            const emissions = factory.newConcept(namespace, 'Emissions');
            emissions.emCO = 10;
            emissions.emHC = 10;
            emissions.emNOx = 10;
            emissions.emHCNOx = 10;
            emissions.emCO2 = 10;
            emissions.emFuelConsumption = 7.5;

            vehicle.emissions = emissions;

            //Adding owner to registry
            Utils.addParticipant(businessNetworkConnection,Person,owner);

            return businessNetworkConnection
                .getAssetRegistry(namespace + '.' + assetType)
                .then(registry => {
                    return registry.add(vehicle);
                  });
          });
    });

  describe('ConfirmOwnership()', () => {
    it('should confirm ownership on a new owner change',
       () => {

            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const newOwner = factory.newResource(namespace,Person,'333333222');
            newOwner.name = "João";
            newOwner.surname = "Cruz";
            newOwner.address = "Lisb";
            const newOwnerRelation =
                factory
                .newRelationship(namespace,Person,newOwner.$identifier);



            return Utils.usePersonIdentity(businessNetworkConnection,owner.fn,
                () => {
                    return Utils.getVehicle(businessNetworkConnection,'V987654321')
                        .then(vehicle => {

                            const ownerRelation =
                                factory
                                .newRelationship(
                                    namespace,
                                    Person,
                                    owner.$identifier);

                            const changeOwner =
                                factory
                                .newTransaction(
                                    namespace,
                                    ChangeOwner);

                            changeOwner.vin = vehicle.$identifier;
                            changeOwner.registrationNumber =
                                vehicle.registrationNumber;
                            changeOwner.make = vehicle.make;

                            const ownership =
                                factory
                                .newConcept(namespace,Ownership);

                            const owners = new Array();
                            owners.push(ownership);
                            ownership.owner = ownerRelation;
                            ownership.newOwner = newOwnerRelation;
                            ownership.share = 1;
                            ownership.state = WAITING;

                            changeOwner.newOwners = owners;

                            return businessNetworkConnection
                                .submitTransaction(changeOwner);

                        });
                }).then(() => {
                    return businessNetworkConnection
                        .connect('admin@bcar-network')
                        .then(() => {
                            return Utils.addParticipant(businessNetworkConnection,Person,newOwner)
                                .then(()=>{
                                    return Utils.usePersonIdentity(businessNetworkConnection,newOwner.fn,
                                        () => {
                                            const confirmOwnership =
                                                factory
                                                .newTransaction(
                                                    namespace,
                                                    ConfirmOwnership);

                                            confirmOwnership.vin = 'V987654321';
                                            confirmOwnership
                                            .registrationNumber = '00-AA-00';
                                            confirmOwnership.make = 'Porsche';
                                            confirmOwnership.newOwner =
                                                newOwnerRelation;

                                            return businessNetworkConnection
                                                .submitTransaction(
                                                    confirmOwnership
                                                ).then(() => {
                                                    return Utils.getVehicle(businessNetworkConnection,
                                                        'V987654321'
                                                        ).then(vehicle => {
                                                            assert.equal(
                                                                vehicle
                                                                .owners[0]
                                                                .owner
                                                                .$identifier,
                                                                newOwner
                                                                .$identifier);
                                                            assert.equal(
                                                                vehicle
                                                                .owners[0]
                                                                .state,
                                                                VALID);
                                                            assert.equal(
                                                                vehicle
                                                                .owners[0]
                                                                .newOwner,
                                                                undefined);
                                                        });
                                                });
                                        });
                                });
                        });
                });
    });
    it('should give an error when confirming ownership on a vehicle not'+
        ' needing confirmation', () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const newOwner = factory.newResource(namespace,Person,'333333222');
            newOwner.name = "João";
            newOwner.surname = "Cruz";
            newOwner.address = "Lisb";
            const newOwnerRelation =
                factory
                .newRelationship(namespace,Person,newOwner.$identifier);

            return Utils.addParticipant(businessNetworkConnection,Person,newOwner)
                .then(() => {
                    return Utils.usePersonIdentity(businessNetworkConnection,newOwner.fn,() => {
                        const confirmOwnership =
                            factory.newTransaction(namespace,ConfirmOwnership);
                        confirmOwnership.vin = 'V987654321';
                        confirmOwnership.registrationNumber = '01-AA-00';
                        confirmOwnership.make = 'Opel';
                        confirmOwnership.newOwner = newOwnerRelation;

                        return businessNetworkConnection
                            .submitTransaction(confirmOwnership)
                            .should.eventually.be
                            .rejectedWith(
                                'There is no ownership confirmation to be done'+
                                ' for vehicle with vin: '+confirmOwnership.vin);
                    });
                });
        });
    it('should give an error when confirming ownership with wrong credentials',
        () => {
            const factory =
                businessNetworkConnection.getBusinessNetwork().getFactory();

            const newOwner = factory.newResource(namespace,Person,'333333222');
            newOwner.name = "João";
            newOwner.surname = "Cruz";
            newOwner.address = "Lisb";
            const newOwnerRelation =
                factory
                .newRelationship(namespace,Person,newOwner.$identifier);

            const fakeOwner = factory.newResource(namespace,Person,'333233222');
            fakeOwner.name = "João";
            fakeOwner.surname = "Santos";
            fakeOwner.address = "Lisb";

            return Utils.usePersonIdentity(businessNetworkConnection,owner.fn,
                () => {
                    return Utils.getVehicle(businessNetworkConnection,'V987654321')
                        .then(vehicle => {

                            const ownerRelation =
                                factory
                                .newRelationship(
                                    namespace,
                                    Person,
                                    owner.$identifier);

                            const changeOwner =
                                factory
                                .newTransaction(
                                    namespace,
                                    ChangeOwner);

                            changeOwner.vin = vehicle.$identifier;
                            changeOwner.registrationNumber =
                                vehicle.registrationNumber;
                            changeOwner.make = vehicle.make;

                            const ownership =
                                factory
                                .newConcept(namespace,Ownership);

                            const owners = new Array();
                            owners.push(ownership);
                            ownership.owner = ownerRelation;
                            ownership.newOwner = newOwnerRelation;
                            ownership.share = 1;
                            ownership.state = WAITING;

                            changeOwner.newOwners = owners;

                            return businessNetworkConnection
                                .submitTransaction(changeOwner);

                        });
                }).then(() => {
                    return businessNetworkConnection
                        .connect('admin@bcar-network')
                        .then(() => {
                            return Utils.addParticipant(businessNetworkConnection,Person,newOwner)
                                .then(() => { return Utils.addParticipant(businessNetworkConnection,Person,fakeOwner);})
                                .then(() => {
                                    // Using wrong person to confirm ownership
                                    return Utils.usePersonIdentity(businessNetworkConnection,fakeOwner.fn,
                                        () => {
                                            const confirmOwnership =
                                                factory
                                                .newTransaction(
                                                    namespace,
                                                    ConfirmOwnership);

                                            confirmOwnership.vin = 'V987654321';
                                            confirmOwnership
                                            .registrationNumber ='01-AA-00';
                                            confirmOwnership.make = 'Opel';
                                            confirmOwnership.newOwner =
                                                newOwnerRelation;

                                            return businessNetworkConnection
                                                .submitTransaction(
                                                    confirmOwnership
                                                ).should.eventually.be
                                                .rejectedWith('Only the new owner can perform confirm ownership');
                                        });
                                });
                        });
                });
        });
  });

});

