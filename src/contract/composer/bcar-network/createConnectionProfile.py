import getopt, sys,json


def help():
    print('Connection Profile creator for HLFv1');
    print('This creator assumes each peer to be on a different machine,')
    print('thus a connection profile is created for each peer/orderer configured.')
    print('')
    print(' Options: ')
    print(' --help  Displays this message.')
    print(' --name=<name of network>  Name of the network')
    print(' --orderers=<nr> Specifies the number of orderers')
    print(' --orgs=<nr> Specifies the number of organizations')
    print(' --peers=<nr> Specifies the number of peers on each organization')




class ConnectionProfile(object):
    def __init__(self):
        super(ConnectionProfile, self).__init__()
        self.name = None
        self.orderers = None
        self.organizations = None
        self.peers = None
        self.ca = 1
        self.namespace = 'example.com'

    def setName(self,name):
        self.name=name

    def setOrderers(self,nr):
        self.orderers = nr

    def setOrganizations(self,nr):
        self.organizations = nr

    def setPeers(self,nr):
        self.peers = nr

    def generateOrdererList(self):
        orderers = []
        for i in range(1,self.orderers+1):
            orderers.append('orderer'+str(i)+'.'+self.namespace)

        return orderers

    def generatePeerList(self,orgName):
        peers = []
        for i in range(self.peers):
            peers.append('peer'+str(i)+'.'+orgName+'.'+self.namespace)

        return peers

    def generateCAList(self,orgName):
        ca = []
        for i in range(self.ca):
            ca.append('ca'+str(i)+'.'+orgName+'.'+self.namespace)

        return ca

    def generateOrgsList(self):
        orgs = []
        for i in range(1,self.organizations+1):
            orgs.append('org'+str(i))

        return orgs

    def generateProfile(self):
        if(self.name == None or self.orderers == None or self.organizations == None or self.peers == None or self.ca == None or self.namespace == None):
            raise Exception('Data to generate profile is missing.')


        profile = {}
        profile['name'] = self.name
        profile['x-type'] = 'hlfv1'
        profile['version'] = '1.0.0'

        channels = {}
        channels['mychannel'] = {}
        channels['mychannel']['orderers'] = self.generateOrdererList()
        channels['mychannel']['peers'] = {}

        organizations = {}

        orderers = {}
        peers = {}
        certificateAuthorities = {}

        for org in self.generateOrgsList():
            orgU = org[0].upper()+org[1:]
            nr = int(org[3:])-1
            organizations[orgU] = {}
            organizations[orgU]['mspid'] = orgU+'MSP'
            organizations[orgU]['peers'] = self.generatePeerList(org)
            organizations[orgU]['certificateAuthorities'] = self.generateCAList(org)

            for peer in organizations[orgU]['peers']:
                # Channel section
                channels['mychannel']['peers'][peer] = {}
                channels['mychannel']['peers'][peer]['endorsingPeer'] = True
                channels['mychannel']['peers'][peer]['chaincodeQuery'] = True
                channels['mychannel']['peers'][peer]['eventSource'] = True
                # Peers section
                peers[peer] = {}
                peers[peer]['url'] = 'grpcs://localhost:'+str(nr+7)+'051'
                peers[peer]['eventUrl'] = 'grpcs://localhost:'+str(nr+7)+'053'
                peers[peer]['grpcOptions'] = {}
                peers[peer]['grpcOptions']['ssl-target-name-override'] = peer
                peers[peer]['tlsCACerts'] = {}
                peers[peer]['tlsCACerts']['pem'] = 'INSERT_'+org.upper()+'_CA_CERT'




            for ca in organizations[orgU]['certificateAuthorities']:
                certificateAuthorities[ca] = {}
                certificateAuthorities[ca]['url'] = "https://localhost:"+str(nr+7)+'054'
                certificateAuthorities[ca]['caName'] = "ca-"+org
                certificateAuthorities[ca]['httpOptions'] = {}
                certificateAuthorities[ca]['httpOptions']['verify'] = False


        for orderer in channels['mychannel']['orderers']:
            orderers[orderer] = {}
            orderers[orderer]['url'] = 'grpcs://localhost:'+'7050'
            orderers[orderer]['grpcOptions'] = {}
            orderers[orderer]['grpcOptions']['ssl-target-name-override'] = orderer
            orderers[orderer]['tlsCACerts'] = {}
            orderers[orderer]['tlsCACerts']['pem'] = 'INSERT_ORDERER_CA_CERT'

        profile['channels'] = channels

        profile['organizations'] = organizations

        profile['orderers'] = orderers

        profile['peers'] = peers

        profile['certificateAuthorities'] = certificateAuthorities

        return profile





unixOptions = "hnrop"
gnuOptions = ["help", "name=", "orderers=","orgs=","peers="]

argumentList = sys.argv[1:]

try:
    arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
    # output error, and return with an error code
    print (str(err))
    sys.exit(2)

profile = ConnectionProfile()

# evaluate given options
for currentArgument, currentValue in arguments:
    if currentArgument in ("-h", "--help"):
        help()
    elif currentArgument in ("-n", "--name"):
        profile.setName(currentValue);
    elif currentArgument in ("-r", "--orderers"):
        profile.setOrderers(int(currentValue));
    elif currentArgument in ("-o", "--orgs"):
        profile.setOrganizations(int(currentValue));
    elif currentArgument in ("-p", "--peers"):
        profile.setPeers(int(currentValue));

templateProfile = profile.generateProfile()

urlList = []
urlList += list(templateProfile['certificateAuthorities'].keys())
urlList += list(templateProfile['peers'].keys())
urlList += list(templateProfile['orderers'].keys())

ips = {}

with open('ipConfigs.json','w') as ipf:
    for url in urlList:
        ips[url] = ''

    json.dump(ips, ipf, indent=4)


with open('prf.json','w') as f:

    json.dump(templateProfile, f, indent=4)
