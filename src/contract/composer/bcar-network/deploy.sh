#!/bin/sh
echo "Delete admin@bcar-network card if exists"
composer card delete -n admin@bcar-network

echo "Starting runtime" 
composer runtime install --card PeerAdmin@hlfv1 --businessNetworkName bcar-network

echo "Deploying network" 
composer network start --card PeerAdmin@hlfv1 --networkAdmin admin --networkAdminEnrollSecret adminpw --archiveFile  bcar-network@0.0.1.bna  --file admin.card


echo "Import admin@bcar-network card"
composer card import -f admin.card


#echo "Creating BNA file"  
#composer archive create --sourceType dir --sourceName . -a 'bcar-network.bna'

#composer runtime install --businessNetworkName bcar-network -c PeerAdmin@hlfv1 

#echo "Updating network"
#composer network update -c admin@bcar-network -a bcar-network.bna
echo "Ping network"
composer network ping -c admin@bcar-network

echo "Launching REST API"
composer-rest-server -c admin@bcar-network -n never -w true
