'use strict';
/**
 * Write your transction processor functions here
 */

var namespace = 'pt.irn.bcar';
var JudicialOfficer = 'JudicialOfficer';
var RegistryEmployee = 'RegistryEmployee';
var Person = 'Person';

var vehicleType = 'Vehicle';
var vehicleClasses = ['LIGHT','HEAVY'];
var DIESEL = 'DIESEL';
var LeaseInfo = "LeaseInfo";
var Loan = "Loan";
var Company = "Company";
var Seizure = "Seizure";
var STATE_WAITING = 'WAITING';
var STATE_VALID = 'VALID';
var STATE_WAITINGCANCELATION = 'WAITINGCANCELATION';
var STATE_ACTIVE = 'ACTIVE';

function onCreateVehicle(createVehicle) {
  var newVehicle = createVehicle.vehicle;
  //throw new Error("Vehicle with same registration Number already exists.");

  var qry = buildQuery(
              'SELECT ' + namespace + '.' + vehicleType +
              ' WHERE (vin == _$vin OR ' +
                      'engine.ein == _$ein OR ' +
                      'registrationNumber == _$registrationNumber)');

  return query(qry,
              {
                  vin: newVehicle.vin ,
                  ein: newVehicle.engine.ein ,
                  registrationNumber: newVehicle.registrationNumber
                })
          .then(function(result) {
              if (result.length >= 1) {
                throw new Error(
                    'Vehicle with same vin or ein or registration ' +
                    'number already exists.');
              }

              switch (newVehicle.category){
                  case 'M':
                  case 'N':
                    if (vehicleClasses.indexOf(newVehicle.vClass) < 0) {
                      throw new Error(
                          'Vehicle from categories M or N ' +
                          'need to have a vehicle class.');
                    }

                  case 'T':
                    if (typeof newVehicle.pwRatio != 'undefined') {
                      throw new Error(
                          'Vehicle from categories M, N or T ' +
                          'can not have pwRatio.');
                    }
                    if (typeof newVehicle.maxLadenMass === 'undefined') {
                      throw new Error(
                          'Vehicle from categories M, N or T ' +
                          'need to have maxLadenMass.');
                    }
                  break;

                  case 'L':
                    if (newVehicle.pwRatio <= 0 ||
                        typeof newVehicle.pwRatio === 'undefined') {
                      throw new Error(
                          'Vehicle from category L ' +
                          'need to have pwRatio.');
                    }
                    if (typeof newVehicle.maxLadenMass != 'undefined') {
                      throw new Error(
                          'Vehicle from category L ' +
                          'can not have maxLadenMass.');
                    }
                }

              if (['L','T'].indexOf(newVehicle.category) >= 0 &&
                  vehicleClasses.indexOf(newVehicle.vClass) >= 0) {
                throw new Error(
                    'Vehicle from categories L or T ' +
                    'can not have a vehicle class.');
              }

              if (newVehicle.engine.fuelType == DIESEL &&
                  (typeof newVehicle.emissions.emParticles === 'undefined' ||
                    typeof newVehicle.emissions.emAbsortionCoefficient ===
                    'undefined')) {
                throw new Error(
                    'Vehicle with DIESEL fuelType engines need ' +
                    'to have emParticles and emAbsortionCoefficient.');
              }

              if (newVehicle.engine.fuelType != DIESEL &&
                (typeof newVehicle.emissions.emParticles != 'undefined' &&
                    typeof newVehicle.emissions.emAbsortionCoefficient !=
                    'undefined')) {
                throw new Error(
                    'Vehicle with non-DIESEL fuelType engines can not' +
                    ' have emParticles and emAbsortionCoefficient.');
              }

              return getAssetRegistry(namespace + '.' + vehicleType)
                  .then(function(vehicleRegistry) {
                        newVehicle.state =  STATE_ACTIVE;
                        vehicleRegistry.add(newVehicle);
                });
            });
}

/**
 * Ownership change transactions
 */

function onChangeOwner(changeOwner) {
  var factory = getFactory();

  return getAssetRegistry(namespace + '.' + vehicleType)
        .then(function(registry) {
            return registry.get(changeOwner.vin)
                .then(function(result) {
                    var vehicleOwnership = new Array();
                    var transactionIssuer =
                        getCurrentParticipant();

                    var ownerShare = 0;

                    for (
                        var i = 0;
                        i < result.owners.length &&
                        result.owners.length != 0;
                        i++) {
                        var ownerRelation = result.owners[i];

                        if(ownerRelation.owner.getFullyQualifiedIdentifier() ==
                            transactionIssuer.getFullyQualifiedIdentifier() || 
                            (ownerRelation.owner.getFullyQualifiedType() == namespace + '.' + Company 
                            && isCompanyOwner(transactionIssuer,ownerRelation.owner) )){
                            vehicleOwnership.push(ownerRelation);
                            ownerShare+=ownerRelation.share;
                            result.owners.splice(i,1);
                            i--;
                        }
                    }


                    if (result.make != changeOwner.make ||
                        result
                        .registrationNumber != changeOwner.registrationNumber){
                        throw new Error(
                            'Change owner data does not match' +
                            ' the vehicle registry.');
                    }else if (ownerShare > 0) {


                        // get sum of new shares added and
                        // pushes new shares to vehicle to be later updated

                        var newTotalShare =
                            changeOwner.newOwners.reduce(
                                function (total,obj) {
                                    if(vehicleOwnership[0].owner.$identifier ==
                                        obj.owner.$identifier){
                                            /* TODO
                                              - Verify if sum of shares is 1
                                              - Verify if certificateHolder
                                                needs to change
                                            */

                                            obj.state = STATE_WAITING;
                                            result.owners.push(obj);
                                            return total + obj.share;
                                    }
                                },0);

                        if ( newTotalShare != ownerShare ){
                            throw new Error(
                                'Can not give more shares than the original'+
                                ' owner has');
                        }
                        return registry.update(result);

                    }else {
                        throw new Error(
                            'Only the owner is capable of changing ' +
                            'vehicle ownership.');
                    }
                });

        });
}

function onConfirmOwnership(confirmOwnership){
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then( function(registry) {
            return registry.get(confirmOwnership.vin)
                .then( async function(vehicle) {
                    var issuer =
                        getCurrentParticipant().getFullyQualifiedIdentifier();
                    if(issuer != confirmOwnership
                                .newOwner
                                .getFullyQualifiedIdentifier() || 
                                (confirmOwnership.newOwner.getFullyQualifiedType() == namespace + '.' + Company 
                                && ! await isCompanyOwner(issuer,confirmOwnership.newOwner))){
                        throw new Error('Only the new owner can perform' +
                            ' confirm ownership.');
                    }
                    var needsUpdate = false;
                    vehicle.owners
                        .forEach( function (ownership) {
                            if(ownership.newOwner != undefined &&
                                (ownership
                                .newOwner
                                .getFullyQualifiedIdentifier() == issuer || 
                                (confirmOwnership.newOwner.getFullyQualifiedIdentifier() == namespace + '.' +Company && 
                                isCompanyOwner(issuer,confirmOwnership.newOwner)))){

                                ownership.owner = ownership.newOwner;
                                ownership.state = STATE_VALID;
                                ownership.newOwner = undefined;
                                needsUpdate = true;
                            }
                        });
                    if(!needsUpdate){
                        throw new Error( 'There is no ownership confirmation' +
                            ' to be done for vehicle with vin: ' + vehicle.vin);
                    }

                    return registry.update(vehicle);
                });
        })
}

function onCancelOwnershipChange(cancelOwnershipChange){
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then( function(registry) {
            return registry.get(cancelOwnershipChange.vin)
                .then( async function(vehicle) {
                    var issuer =
                        getCurrentParticipant();
                    if(issuer != cancelOwnershipChange
                                .owner
                                .getFullyQualifiedIdentifier() && 
                                (cancelOwnershipChange.owner.getFullyQualifiedType() == namespace + '.' + Company 
                                && ! await isCompanyOwner(issuer,cancelOwnershipChange.owner)) && ! await isOwner(issuer,vehicle)){
                        throw new Error('Only the new owner can perform' +
                            ' confirm ownership.');
                    }
                    var needsUpdate = false;
                    vehicle.owners
                        .forEach( function (ownership) {
                            if(ownership.newOwner != undefined &&
                                (ownership
                                .newOwner
                                .getFullyQualifiedIdentifier() == issuer.getFullyQualifiedIdentifier() || 
                                (cancelOwnershipChange.owner.getFullyQualifiedIdentifier() == namespace + '.' +Company && 
                                isCompanyOwner(issuer,cancelOwnershipChange.owner) || 
                                ownership.owner.getFullyQualifiedIdentifier() == issuer.getFullyQualifiedIdentifier()))){

                                ownership.state = STATE_VALID;
                                ownership.newOwner = undefined;
                                needsUpdate = true;
                            }
                        });
                    if(!needsUpdate){
                        throw new Error( 'There is no ownership confirmation' +
                            ' to be done for vehicle with vin: ' + vehicle.vin);
                    }

                    return registry.update(vehicle);
                });
            });
}


/**
 *  Lease transactions
 */

function onCreateLease(createLease){
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then(function (registry) {
            return registry.get(createLease.vin)
                .then( async function (result) {
                    var factory = getFactory();

                    var now = new Date();

                    var issuer = getCurrentParticipant();
                    if (! await isOwner(issuer,result)) {
                        throw new Error(
                            'Only the owner can create a lease contract');
                    }

                    if (createLease.startDate.getTime() >=
                        createLease.endDate.getTime()) {
                        throw new Error(
                            'End date must be greater than start date.');
                    }
                    if (now >= createLease.endDate.getTime()) {
                        throw new Error(
                            'End date must be greater than current date.');
                    }
                    if (result.registrationNumber !=
                        createLease.registrationNumber ||
                        result.make != createLease.make) {
                        throw new Error(
                            'Create Lease data does not match the registry.');
                    }

                    var lease = factory.newConcept(namespace,LeaseInfo);
                    lease.issuer =
                        factory
                        .newRelationship(namespace,Person,issuer.$identifier);
                    lease.lessee = createLease.lessee;
                    lease.startDate = createLease.startDate;
                    lease.endDate = createLease.endDate;
                    lease.totalValue = createLease.totalValue;
                    lease.state = STATE_WAITING;
                    result.lease = lease;

                    return registry.update(result);
                });
        });
}

function onConfirmLease(acceptLease){
    var issuer = getCurrentParticipant();
    return getAssetRegistry(namespace+'.'+vehicleType)
        .then( registry => {
            return registry.get(acceptLease.vin)
                .then( async vehicle => {
                    if ( vehicle.lease == undefined || ( ! (await isLessee(issuer,vehicle)) &&
                        !(await isRegistryEmployee(issuer)))){
                        throw Error('Only authorized participants can perform this action.')
                    }

                    vehicle.lease.state = STATE_VALID;
                    return registry.update(vehicle);
                });
        })
}

function onCancelLease(cancelLease){
    var issuer = getCurrentParticipant();
    return getAssetRegistry(namespace+'.'+vehicleType)
        .then( registry => {
            return registry.get(cancelLease.vin)
                .then(async vehicle => {
                    if(await isJudicialOfficer(issuer) ||
                            await isRegistryEmployee(issuer)){
                        vehicle.lease = undefined;
                        return registry.update(vehicle);
                    }

                    if(!(await isOwner(issuer,vehicle) ||
                            await isLessee(issuer,vehicle))){
                        throw new Error(
                            'Only the owner or the lessee can issue a lease'+
                            ' termination.');
                    }
                    if(vehicle.lease == undefined){
                        throw new Error(
                            'Vehicle does not have any active lease.');
                    }
                    vehicle.lease.issuer =
                        getFactory()
                        .newRelationship(namespace,Person,issuer.$identifier);
                    
                    if(vehicle.lease.state == STATE_WAITING){
                        vehicle.lease = undefined;
                    } else {
                        vehicle.lease.state = STATE_WAITINGCANCELATION;
                    }
                    return registry.update(vehicle);
                });
        })
}

function onConfirmLeaseTermination(confirmLeaseTermination){
    var issuer = getCurrentParticipant();
    return getAssetRegistry(namespace+'.'+vehicleType)
        .then( registry => {
            return registry.get(confirmLeaseTermination.vin)
                .then(async vehicle => {
                    if( !(await isOwner(issuer,vehicle)) &&
                        !(await isLessee(issuer,vehicle))){
                        throw new Error(
                            'Only the owner or the lessee can confirm a lease' +
                            ' termination.');
                    }
                    if(vehicle.lease == undefined){
                        throw new Error(
                            'Vehicle does not have any active lease.');
                    }

                    if(vehicle.lease.state != STATE_WAITINGCANCELATION){
                        throw new Error(
                            'A Cancel Lease Transaction needs to be' +
                            ' issued first.');
                    }

                    if(vehicle.lease.issuer.getFullyQualifiedIdentifier() ==
                        issuer.getFullyQualifiedIdentifier()){

                        if(vehicle.lease.lessee.getFullyQualifiedIdentifier() ==
                         issuer.getFullyQualifiedIdentifier()){
                            throw new Error('This action needs to be taken by the leaser.');

                        } else {
                            throw new Error('This action needs to be taken by the lessee.');
                        }
                    }

                    vehicle.lease = undefined;
                    return registry.update(vehicle);
                });
        });
}

function onCancelLeaseTermination(cancelLeaseTermination){
    var issuer = getCurrentParticipant();
    return getAssetRegistry(namespace+'.'+vehicleType)
        .then( registry => {
            return registry.get(cancelLeaseTermination.vin)
                .then(async vehicle => {
                    if(!(await isOwner(issuer,vehicle) ||
                        await isLessee(issuer,vehicle))){
                        throw new Error(
                            'Only the owner or the lessee can confirm a lease' +
                            ' termination');
                    }

                    if(vehicle.lease == undefined){
                        throw new Error(
                            'Vehicle does not have any active lease.');
                    }

                    if(vehicle.lease.state != STATE_WAITINGCANCELATION ||
                        vehicle.lease.issuer.getFullyQualifiedIdentifier() !=
                        issuer.getFullyQualifiedIdentifier()){
                        throw new Error(
                            'A lease termination operation must' +
                            ' be issued first.');
                    }

                    vehicle.lease.state = STATE_VALID;
                    vehicle.lease.issuer =
                        getFactory()
                        .newRelationship(namespace,Person,issuer.$identifier);
                    return registry.update(vehicle);
                });
            });
}

/*
    Guarantee transactions
*/

function onRegisterAsGuarantee(registerAsGuarantee){
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then(function (registry) {
            return registry.get(registerAsGuarantee.vin)
                .then( async function (result){
                    var issuer = getCurrentParticipant();
                    if(! await isOwner(issuer,result)){
                        throw new Error('Only the owner is capable of' +
                            ' registering the vehicle as loan garantee.');
                    }

                    if(result.seizure != undefined){
                        throw new Error(
                            'Vehicle should not have a pending seizure in place');
                    }

                    var factory = getFactory();
                    var coll = factory.newConcept(namespace,Loan);
                    coll.type = registerAsGuarantee.type;
                    coll.creditor = registerAsGuarantee.creditor;
                    coll.totalValue = registerAsGuarantee.totalValue;
                    coll.penalty = registerAsGuarantee.penalty;
                    if(result.loan == null){
                        result.loan = coll;
                        return registry.update(result);
                    }else{
                        throw new Error('Vehicle with registration number ' +
                                    registerAsGuarantee.registrationNumber +
                                    ' was already registered as loan garantee.');
                    }
                })
        })
}

function onCancelGuarantee(cancelGuarantee){
    var issuer = getCurrentParticipant();
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then(function (registry) {
            return registry.get(cancelGuarantee.vin)
                .then( async vehicle => {
                    var needsUpdate = false;
                    if( vehicle
                        .registrationNumber != cancelGuarantee
                                                .registrationNumber &&
                        vehicle.make != cancelGuarantee.make){
                        throw new Error('Vehicle does not match the registy.')
                    }
                    if(await isOwner(issuer,vehicle)){
                        if(vehicle.loan == undefined){
                            throw new Error('This Vehicle is not registered '+
                                'as a Garantee.');
                        }
                        needsUpdate = true;
                        vehicle.loan.waitingCancelation = true;
                    }
                    if(await isCreditor(issuer,vehicle)){
                        needsUpdate = true;
                        vehicle.loan = null;
                    }
                    if(needsUpdate){
                        return registry.update(vehicle);
                    }else{
                        return null;
                    }
                });
        });
}

function onConfirmGuaranteeCancelation(confirmGuaranteeCancelation){
    var issuer = getCurrentParticipant();
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then( function (registry) {
            return registry.get(confirmGuaranteeCancelation.vin)
                .then( async vehicle => {
                    if(!(await isCreditor(issuer,vehicle)) && !(await isRegistryEmployee(issuer))){
                        throw new Error('Only the Creditor can execute this action');
                    }

                    if( vehicle
                        .registrationNumber != confirmGuaranteeCancelation
                                                .registrationNumber &&
                        vehicle.make != confirmGuaranteeCancelation.make){
                        throw new Error('Vehicle does not match the registry.');
                    }

                    if(vehicle.loan == undefined ||
                        vehicle.loan.waitingCancelation == false){
                        throw new Error('No Loan defined or the garantee is '+
                            'not in cancelation process.');
                    }
                    vehicle.loan = null;
                    return registry.update(vehicle);
                });
        });
}

function onRejectGuaranteeCancelation(rejectGuaranteeCancelation){
    var issuer = getCurrentParticipant();
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then( function (registy){
            return registy.get(rejectGuaranteeCancelation.vin)
                .then( async vehicle => {
                    if(!(await isCreditor(issuer,vehicle)) && !(await isRegistryEmployee(issuer))){
                        throw new Error('Only the Creditor can execute this action');
                    }

                    if( vehicle
                        .registrationNumber != rejectGuaranteeCancelation
                                                .registrationNumber &&
                        vehicle.make != rejectGuaranteeCancelation.make){
                        throw new Error('Vehicle does not match the registry.');
                    }

                    if(vehicle.loan == undefined ||
                        vehicle.loan.waitingCancelation == false){
                        throw new Error('No Loan defined or the garantee is '+
                            'not in cancelation process.');
                    }
                    vehicle.loan.waitingCancelation = false;
                    return registy.update(vehicle);
                });
        });
}

/**
 * Seizure transactions
 */

function onIssuePendingSeizure(issuePendingSeizure){
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then( registry => {
            return registry.get(issuePendingSeizure.vin)
                .then( vehicle => {
                    if( vehicle
                        .registrationNumber != issuePendingSeizure
                                                .registrationNumber &&
                        vehicle.make != issuePendingSeizure.make){
                        throw new Error('Vehicle does not match the registry.');
                    }
                    if(vehicle.seizure != undefined){
                        throw new Error(
                            'Vehicle already has a pending Seizure');
                    }

                    if(vehicle.loan != undefined &&
                        vehicle.loan.creditor.getFullyQualifiedIdentifier() !=
                            issuePendingSeizure
                            .creditor.getFullyQualifiedIdentifier() &&
                            vehicle.loan.type == 'COLLATERAL'){
                        throw new Error(
                            'Vehicle is subject to a loan as collateral');
                    }
                    var factory = getFactory();

                    var seizure = factory.newConcept(namespace,Seizure);
                    seizure.owner = issuePendingSeizure.owner;
                    seizure.creditor = issuePendingSeizure.creditor;
                    seizure.totalValue = issuePendingSeizure.totalValue;
                    seizure.status = STATE_WAITING;

                    vehicle.seizure = seizure;
                    return registry.update(vehicle);

                });
        });
}

function onIssueSeizure(issueSeizure){
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then( registry => {
            return registry.get(issueSeizure.vin)
                .then( vehicle => {
                    if(vehicle.loan != undefined){
                        if( vehicle.loan.creditor.getFullyQualifiedIdentifier() !=
                            issueSeizure.creditor.getFullyQualifiedIdentifier() &&
                            vehicle.loan.type == 'COLLATERAL'){
                        throw new Error(
                            'Vehicle is subject to a loan as collateral');
                        } else {
                            vehicle.loan = undefined;
                        }
                    }
                    var oldOwner;
                    var creditor;
                    if(vehicle.seizure != undefined && vehicle.seizure.status == STATE_WAITING){
                        oldOwner = vehicle.seizure.owner;
                        creditor = vehicle.seizure.creditor;

                        if(oldOwner.getFullyQualifiedIdentifier() != issueSeizure.owner.getFullyQualifiedIdentifier() ||
                            creditor.getFullyQualifiedIdentifier() != issueSeizure.creditor.getFullyQualifiedIdentifier()){
                            throw new Error('A Pending Seizure is in place for a different Owner or Creditor.');
                        }
                        vehicle.seizure = undefined;

                    } else {
                        oldOwner = issueSeizure.owner;
                        creditor = issueSeizure.creditor;
                    }

                    vehicle.owners
                        .forEach( function (ownership) {
                            if(ownership.owner.getFullyQualifiedIdentifier() ==
                                oldOwner.getFullyQualifiedIdentifier()){
                                ownership.owner = creditor;
                                ownership.state = STATE_VALID;
                                ownership.newOwner = undefined;
                            }
                        });


                    return registry.update(vehicle);
            });
        });
}

function onCancelSeizure(cancelSeizure){
    return getAssetRegistry(namespace + '.' + vehicleType)
        .then( registry => {
            return registry.get(cancelSeizure.vin)
                .then( vehicle => {
                    vehicle.seizure = undefined;
                    return registry.update(vehicle);
                });
        });

}

/**
 * Change State transactions
 */

function onChangeState(changeState){
    return getAssetRegistry(namespace+'.'+vehicleType)
        .then( registry => {
            return registry.get(changeState.vin)
                .then( vehicle => {
                    vehicle.state = changeState.newState;

                    return registry.update(vehicle);
                });
        });
}

/**
 * Permissions checking functions
 */

async function isOwner(person,vehicle){
    var finalResult = false;
    var promiseList = [];
    for (var i = 0 ; vehicle.owners != undefined && i < vehicle.owners.length; i++) {
        var owner = vehicle.owners[i].owner;
        if(owner.getFullyQualifiedIdentifier() == person.getFullyQualifiedIdentifier()){
            finalResult = true;
        }else if(owner.getFullyQualifiedType() == namespace + '.' + Company){
            var promise = isCompanyOwner(person,owner).then( (result) => {
                if(result){
                    finalResult = result;
                }
            });
            promiseList.push(promise);
        }else{
            finalResult = false;
        }
    }
    await Promise.all(promiseList);
    return finalResult;
}

async function isLessee(person,vehicle){
    return (vehicle.lease != undefined && (vehicle.lease.lessee.getFullyQualifiedIdentifier() ==
            person.getFullyQualifiedIdentifier() ||
        (vehicle.lease.lessee.getFullyQualifiedType() ==
            namespace + '.' + Company &&
            await isCompanyOwner(person,vehicle.lease.lessee))));
}

function isCompanyOwner(person,company){
    return getParticipantRegistry(namespace+'.'+Company).then( registry => {
        //FIXME use fully qualified identifier
        return registry.get(company.$identifier).then( cmpy => {
            var result = false;
            cmpy.owners.forEach( (owner) => {
                if(owner.getFullyQualifiedIdentifier() == person.getFullyQualifiedIdentifier()){
                    result = true;
                }
            });

            return result;
        });
    });
}

async function isRegistryEmployee(person){
    return person.getFullyQualifiedIdentifier() == namespace + '.' + RegistryEmployee;
}
async function isCreditor(person,vehicle){
    return vehicle.loan != undefined &&
        ((vehicle.loan.creditor.getFullyQualifiedType() == namespace + '.' + Person &&
        vehicle.loan.creditor.getFullyQualifiedIdentifier() == person.getFullyQualifiedIdentifier()) ||
         await isCompanyOwner(person,vehicle.loan.creditor));

}

async function isJudicialOfficer(person){
    return person.getFullyQualifiedType() == namespace + '.' + JudicialOfficer;
}