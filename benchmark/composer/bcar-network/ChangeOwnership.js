'use strict';

module.exports.info  = 'BCar Network Performance Test';
var randomstring = require('randomstring');
const composerUtils = require('../../../src/composer/composer_utils');
const removeExisting = require('../composer-test-utils').clearAll;
const Log = require('../../../src/comm/util').log;
const os = require('os');

const namespace = 'pt.irn.bcar';
const Person = 'Person';
const VALID = 'VALID';
const WAITING = 'WAITING';
const STATE_ACTIVE = 'ACTIVE';
const Ownership = 'Ownership';
const ChangeOwner = 'ChangeOwner';


const busNetName = 'bcar-network';
const uuid = os.hostname() + process.pid; // UUID for client within test

let bc;                 // The blockchain main (Composer)
let busNetConnections;  // Global map of all business network connections to be used
let testAssetNum;       // Number of test assets to create
let factory;            // Global Factory
let owner;
let newOwner;
let assets = Array();
let userName = 'User1_' + uuid;

module.exports.init = async function(blockchain, context, args) {
    // Create Participants and Assets to use in main test
    bc = blockchain;
    busNetConnections = new Map();
    busNetConnections.set('admin', context);
    testAssetNum = args.testAssets;

    let participantRegistry = await busNetConnections.get('admin').getParticipantRegistry(namespace + '.Person');
    let assetRegistry = await busNetConnections.get('admin').getAssetRegistry(namespace + '.Vehicle');
    

    try {
        factory = busNetConnections.get('admin').getBusinessNetwork().getFactory();

        let exists = await participantRegistry.exists('' + uuid);
        if (!exists) {
            // Example for creating multiple participants for test use
            // Create & add participant
            owner = factory.newResource(namespace, 'Person', '' + uuid);
            owner.surname = 'Santos';
            owner.name = 'José';
            owner.address = 'Liberdade 21';

            
            await participantRegistry.add(owner);

            //Log('About to create new participant card');
            //let userName = 'User1_' + uuid;
            //let newConnection = await composerUtils.obtainConnectionForParticipant(busNetConnections.get('admin'), busNetName, participant, userName);
            //busNetConnections.set(userName, newConnection);
        } else {
            owner = await participantRegistry.get(''+uuid);
        }

        for( var i = 0; i < testAssetNum; i++){
            const ownerRelation =
            factory.newRelationship(namespace, Person, owner.$identifier);
            
            var id  = randomstring.generate(10) + i;
            sampleVehicle =
                factory.newResource(namespace, 'Vehicle', id);
            sampleVehicle.vin = sampleVehicle.$identifier;
            sampleVehicle.registrationNumber = id;
            sampleVehicle.make = 'Opel';
            sampleVehicle.typeVersion = 'Corsa';
            sampleVehicle.category = 'M';
            sampleVehicle.vClass = 'LIGHT';
            sampleVehicle.state = STATE_ACTIVE;


            const engineS = factory.newConcept(namespace, 'Engine');
            engineS.capacity = 0;
            engineS.fuelType = 'GASOLINE';
            engineS.ratedSpeed = 0;
            engineS.ein = 'E11'+id;

            sampleVehicle.engine = engineS;
            sampleVehicle.maxLadenMass = 3500;
            
            const ownerRelation =
                factory
                .newRelationship(
                    namespace,
                    Person,
                    owner.$identifier);

            const ownership = factory.newConcept(namespace, Ownership);
            const owners = new Array();
            owners.push(ownership);
            ownership.owner = ownerRelation;
            ownership.share = 1;
            ownership.state = VALID;

            sampleVehicle.owners = owners;
            sampleVehicle.certificateHolder = ownerRelation;

            const emissionsSample = factory.newConcept(namespace, 'Emissions');
            emissionsSample.emCO = 10;
            emissionsSample.emHC = 10;
            emissionsSample.emNOx = 10;
            emissionsSample.emHCNOx = 10;
            emissionsSample.emCO2 = 10;
            emissionsSample.emFuelConsumption = 7.5;

            sampleVehicle.emissions = emissionsSample;
            
            assets.push(sampleVehicle);
        } 




        newOwner =
            factory
            .newResource(namespace,Person,'' + uuid);
        newOwner.name = "João";
        newOwner.surname = "Cruz";
        newOwner.address = "Lisb";
        let exists = await participantRegistry.exists('' + uuid);
        if(!exists){
            await participantRegistry.add(owner);
        }

        
        let newConnection = await composerUtils.obtainConnectionForParticipant(busNetConnections.get('admin'), busNetName, owner, userName);
        busNetConnections.set(userName, newConnection);

        // Conditionally add/update Test Assets
        let populated = await assetRegistry.exists(assets[0].getIdentifier());
        if (populated) {
            Log('Removing test assets ...');
            await removeExisting(assetRegistry, id);
        }

        await assetRegistry.addAll(assets);
    } catch (error) {
        Log('error in test init(): ', error);
        return Promise.reject(error);
    }
};

module.exports.run = function() {
    var vehicle = assets[--testAssetNum];
    const newOwnerRelation =
        factory
        .newRelationship(
            namespace,
            Person,
            newOwner.$identifier);

    const changeOwner =
        factory
        .newTransaction(
            namespace,
            ChangeOwner);
    changeOwner.vin = vehicle.$identifier;
    changeOwner.registrationNumber =
        vehicle.registrationNumber;
    changeOwner.make = vehicle.make;

    const ownership =
        factory
        .newConcept(namespace, Ownership);
    const owners = new Array();
    owners.push(ownership);
    ownership.owner = ownerRelation;
    ownership.newOwner = newOwnerRelation;
    ownership.share = 1;
    ownership.state = WAITING;

    changeOwner.newOwners = owners;

    return bc.bcObj.submitTransaction(busNetConnections.get(userName), changeOwner);
};

module.exports.end = function() {
    return Promise.resolve(true);
};